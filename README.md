# Leetcode C++ Sources

## Testing

1. Install dependencies  
   ```bash
   sudo apt-get install build-essential cmake python3-jinja2
   ```
2. Generate tests
   ```bash
   ./configure.py
   ```
3. Build with [CMake](https://cmake.org)  
   ```bash
   cmake -B build
   make -C build -j `nproc`
   ```
3. Run tests  
   ```bash
   build/test_main
   ```
