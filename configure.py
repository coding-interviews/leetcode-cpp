#!/usr/bin/env python3
import argparse
import logging
import os
import sys
from glob import glob
from multiprocessing.pool import Pool
from typing import List, Dict

import jinja2
import pygccxml

sys.setrecursionlimit(0x7fffffff)

# The solution of these problems are unordered
UNORDERED = [140, 187, 212, 301, 491]

def parse_function(filenames: str) -> Dict[str, pygccxml.declarations.calldef_members.member_function_t]:
    generator_path, generator_name = pygccxml.utils.find_xml_generator()
    xml_generator_config = pygccxml.parser.xml_generator_configuration_t(
        xml_generator_path=generator_path, xml_generator=generator_name
    )
    content = []
    for i, filename in enumerate(filenames):
        with open(filename, 'r', encoding="utf-8") as f:
            content.append(f"namespace solution{i} {{\n{f.read()}}}\n")
    content = rf"""
    #include <bits/stdc++.h>
    using namespace std;
    {''.join(content)}
    """
    decls = pygccxml.parser.parse_string(content, xml_generator_config)
    global_namespace = pygccxml.declarations.get_global_namespace(decls)

    jinja2_args = {}
    for i, filename in enumerate(filenames):
        classes = global_namespace.namespace(f"solution{i}").declarations
        matcher = pygccxml.declarations.declaration_matcher(name="Solution", decl_type=pygccxml.declarations.class_t)
        solution_class = pygccxml.declarations.matcher.get_single(matcher, classes)
        problem_id = os.path.splitext(os.path.basename(filename))[0]
        jinja2_args[problem_id] = solution_class.public_members[0]
    return jinja2_args


def main(args):
    logging.basicConfig(level=logging.INFO,
                        format="[%(levelname)s] %(asctime)s %(message)s")
    jinja2_args = parse_function(glob(args.input))

    with open('test/template.jinja2', 'r', encoding='utf-8') as f:
        template = f.read()

    source = jinja2.Template(source=template).render(args=jinja2_args, unordered=set(map(str, UNORDERED)))

    with open('test/test_main.cpp', 'w', encoding='utf-8') as f:
        f.write(source)


if __name__ == "__main__":
    args = argparse.ArgumentParser()
    args.add_argument("-i", "--input", default="src/*.cpp", type=str, help="input file name")
    args.add_argument("-o", "--output", default="test", type=str, help="output directory name")
    main(args.parse_args())
