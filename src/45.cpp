class Solution
{
public:
    int jump(vector<int> &nums)
    {
        int N = nums.size(), result = 0, cur = 0;
        while (cur < N - 1) // cur==N-1, 即站在最后一个位置上, 就不需要再跳了
        {
            // 如果终点在cur跳力所及的范围内，即无需再选择下一个点，可以直接结束
            if (cur + nums[cur] >= N - 1)
                return result + 1;
            // 从cur出发，在cur跳力所及的范围内
            // 选择一个跳的最远的点next
            int next = 0, maxpos = 0;
            for (int i = cur + 1; i <= cur + nums[cur]; i++)
                if (i + nums[i] > maxpos)
                    maxpos = i + nums[i], next = i;
            // 从cur跳到next
            cur = next;
            result++;
        }
        return result;
    }
};
