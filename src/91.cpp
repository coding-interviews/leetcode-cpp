class Solution
{
public:
    int numDecodings(string &s)
    {
        if (s.empty())
            return 0;
        vector<int> dp(s.length() + 1);
        dp[0] = 1, dp[1] = s[0] > '0' ? 1 : 0;
        for (int i = 1; i < s.length(); i++)
            if (s[i] == '0')
                dp[i + 1] = s[i - 1] == '1' || s[i - 1] == '2' ? dp[i - 1] : 0;
            else if (s[i - 1] == '1' || s[i - 1] == '2' && s[i] <= '6')
                dp[i + 1] = dp[i - 1] + dp[i];
            else
                dp[i + 1] = dp[i];
        return dp.back();
    }
};
