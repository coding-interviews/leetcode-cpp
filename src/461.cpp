/*
 * @lc app=leetcode id=461 lang=cpp
 *
 * [461] Hamming Distance
 */

// @lc code=start
class Solution
{
public:
    int hammingDistance(int x, int y)
    {
        int z = x ^ y, hamming = 0;
        while(z)
        {
            hamming++;
            z = z & (z - 1);
        }
        return hamming;
    }
};
// @lc code=end
