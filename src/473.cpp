/*
 * @lc app=leetcode id=473 lang=cpp
 *
 * [473] Matchsticks to Square
 */

// @lc code=start
static int _ = []()
{
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    return 0;
}();

class Solution
{
private:
    bool subarrayEqualsK(vector<int> &nums, vector<int> &subarray, int i, int target)
    {
        if(i >= nums.size())
            return target == 0;
        subarray.push_back(nums[i]);
        if(subarrayEqualsK(nums, subarray, i + 1, target - nums[i]))
            return true;
        subarray.pop_back();
        return subarrayEqualsK(nums, subarray, i + 1, target);
    }

public:
    bool makesquare(vector<int>& nums)
    {
        int sum = accumulate(nums.begin(), nums.end(), 0);
        if(sum % 4)
            return false;
        sort(nums.begin(), nums.end(), greater<int>());
        for(int i = 0; i < 4; i++)
        {
            vector<int> subarray;
            if(nums.empty() || !subarrayEqualsK(nums, subarray, 0, sum / 4))
                return false;
            for(int num: subarray)
                nums.erase(find(nums.begin(), nums.end(), num));
        }
        return nums.empty();
    }
};
// @lc code=end
