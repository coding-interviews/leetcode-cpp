/*
 * @lc app=leetcode id=474 lang=cpp
 *
 * [474] Ones and Zeroes
 */

// @lc code=start
class Solution
{
public:
    int findMaxForm(vector<string> &strs, int m, int n)
    {
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        for (int i = 1; i <= strs.size(); i++)
        {
            int count0 = count(strs[i - 1].begin(), strs[i - 1].end(), '0');
            int count1 = count(strs[i - 1].begin(), strs[i - 1].end(), '1');
            for (int wm = m; wm >= count0; wm--)
                for (int wn = n; wn >= count1; wn--)
                    dp[wm][wn] = max(dp[wm][wn], dp[wm - count0][wn - count1] + 1);
        }
        return dp.back().back();
    }
};

// @lc code=end
