class Solution
{
private:
    void combine(vector<vector<int>> &result, vector<int> &path, int n, int i, int k)
    {
        if (k <= 0)
        {
            result.push_back(path);
            return;
        }
        for (int cur = i; cur <= n; cur++)
        {
            path.push_back(cur);
            combine(result, path, n, cur + 1, k - 1);
            path.pop_back();
        }
    }

public:
    vector<vector<int>> combine(int n, int k)
    {
        vector<vector<int>> result;
        vector<int> path;
        combine(result, path, n, 1, k);
        return result;
    }
};
