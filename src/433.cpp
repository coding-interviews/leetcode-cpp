/*
 * @lc app=leetcode id=433 lang=cpp
 *
 * [433] Minimum Genetic Mutation
 */

// @lc code=start
class Solution
{
private:
    /**
     * @brief 在dict中找出所有与start相差近一个字母的单词
     */
    vector<string> findNext(const unordered_set<string> &dict, const string &start)
    {
        vector<string> result;
        string next = start;
        for(int pos = 0; pos < start.length(); pos++)
        {
            char backup = next[pos];
            for(char ch: {'A', 'C', 'G', 'T'})
            {
                next[pos] = ch;
                if(dict.count(next))
                    result.push_back(next);
                next[pos] = backup;
            }
        }
        return result;
    }

public:
    int minMutation(string &start, string &end, vector<string>& bank)
    {
        unordered_set<string> dict;
        for(const string &word: bank)
            dict.insert(word);

        unordered_set<string> visited;
        queue<string> q({start});
        for(int steps = 0; !q.empty(); steps++)
            for(int i = q.size(); i > 0; i--)
            {
                string front = move(q.front());
                q.pop();
                if(front == end)
                    return steps;
                if(visited.count(front))
                    continue;
                visited.insert(front);
                for(const string &next: findNext(dict, front))
                    if(!visited.count(next))
                        q.push(next);
            }
        return -1;
    }
};
// @lc code=end
