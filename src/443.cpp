/*
 * @lc app=leetcode id=443 lang=cpp
 *
 * [443] String Compression
 */

// @lc code=start
class Solution
{
private:
    void push_count(vector<char> &result, int count)
    {
        for(char ch: to_string(count))
            result.push_back(ch);
    }

public:
    int compress(vector<char>& chars)
    {
        if(chars.empty())
            return 0;
        vector<char> result;
        result.reserve(chars.size());
        int count = 1;
        for(int i = 1; i <= chars.size(); i++)
        {
            if(i == chars.size() || chars[i - 1] != chars[i])
            {
                result.push_back(chars[i - 1]);
                if(count > 1)
                    push_count(result, count);
                count = 1;
            }
            else
                count++;
        }
        chars = move(result);
        return chars.size();
    }
};
// @lc code=end
