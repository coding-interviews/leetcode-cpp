class Solution
{
private:
    bool valid(string &&byte)
    {
        if (byte.front() == '0')
            return byte.length() == 1; // 0: OK, 00: Not OK
        int number = atoi(byte.c_str());
        return number < 256;
    }

    void dfs(vector<string> &result, string &path, string &s, int start, int nbyte)
    {
        if (nbyte == 0)
        {
            if (start >= s.length())
            {
                path.pop_back();
                result.push_back(path);
                path.push_back('.');
            }
            return;
        }
        for (int next = start + 1; next <= min(start + 3, (int)s.length()); next++)
        {
            if (!valid(s.substr(start, next - start)))
                continue;
            for (int i = start; i < next; i++)
                path.push_back(s[i]);
            path.push_back('.');
            dfs(result, path, s, next, nbyte - 1);
            path.pop_back();
            for (int i = start; i < next; i++)
                path.pop_back();
        }
    }

public:
    vector<string> restoreIpAddresses(string &s)
    {
        vector<string> result;
        string path;
        dfs(result, path, s, 0, 4);
        return result;
    }
};
