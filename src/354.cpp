/*
 * @lc app=leetcode id=354 lang=cpp
 *
 * [354] Russian Doll Envelopes
 */

// @lc code=start
static int _ = []()
{
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    return 0;
}();

class Solution
{
private:
	int lengthOfLIS(vector<int>& nums)
	{
		vector<int> dp(nums.size(), INT_MAX);
		for (int num : nums)
			*lower_bound(dp.begin(), dp.end(), num) = num;
		return lower_bound(dp.begin(), dp.end(), INT_MAX) - dp.begin();
	}

public:
	int maxEnvelopes(vector<vector<int>>& envelopes)
    {
        sort(envelopes.begin(), envelopes.end(), [](const vector<int> &p1, const vector<int> &p2)
        {
            return p1[0] < p2[0] || p1[0] == p2[0] && p1[1] > p2[1];
        });
        vector<int> nums;
        for (const vector<int>& envelope : envelopes)
            nums.push_back(envelope[1]);
        return lengthOfLIS(nums);
	}
};
// @lc code=end
