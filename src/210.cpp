/*
 * @lc app=leetcode id=210 lang=cpp
 *
 * [210] Course Schedule II
 */

// @lc code=start
class Solution
{
public:
    vector<int> findOrder(int numCourses, vector<vector<int>> &prerequisites)
    {
        // 生成邻接表，统计每个点的入度
        vector<vector<int>> edges(numCourses); // 邻接表
        vector<int> indegree(numCourses, 0);   // 入度
        for (const vector<int> &entry : prerequisites)
        {
            edges[entry[1]].push_back(entry[0]);
            indegree[entry[0]]++;
        }

        // 维护一个入度为0的集合
        queue<int> q;
        for (int i = 0; i < numCourses; i++)
            if (indegree[i] == 0)
                q.push(i);

        // 拓扑排序
        vector<int> result;
        while (!q.empty())
        {
            int p = q.front();
            q.pop();
            result.push_back(p);
            for (int next : edges[p])
            {
                indegree[next]--;
                if (indegree[next] == 0)
                    q.push(next);
            }
        }
        return result.size() == numCourses ? result : vector<int>();
    }
};
// @lc code=end
