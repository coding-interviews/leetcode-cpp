/*
 * @lc app=leetcode id=27 lang=cpp
 *
 * [27] Remove Element
 */

// @lc code=start
class Solution
{
public:
    int removeElement(vector<int>& nums, int val)
    {
        int right = nums.size() - 1;
        for(int left = nums.size() - 1; left >= 0; left--)
            if(nums[left] == val)
                swap(nums[left], nums[right--]);
        return right + 1;
    }
};
// @lc code=end
