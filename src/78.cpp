class Solution
{
private:
    void subsets(vector<vector<int>> &result, vector<int> &path, const vector<int> &nums, int i)
    {
        result.push_back(path);
        for (int cur = i; cur < nums.size(); cur++)
        {
            path.push_back(nums[cur]);
            subsets(result, path, nums, cur + 1);
            path.pop_back();
        }
    }

public:
    vector<vector<int>> subsets(vector<int> &nums)
    {
        sort(nums.begin(), nums.end());
        vector<vector<int>> result;
        vector<int> path;
        subsets(result, path, nums, 0);
        return result;
    }
};
