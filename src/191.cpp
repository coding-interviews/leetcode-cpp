/*
 * @lc app=leetcode id=191 lang=cpp
 *
 * [191] Number of 1 Bits
 */

// @lc code=start
class Solution
{
public:
    int hammingWeight(uint32_t n)
    {
        int result = 0;
        while(n)
        {
            n = (n - 1) & n;
            result++;
        }
        return result;
    }
};
// @lc code=end
