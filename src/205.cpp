/*
 * @lc app=leetcode id=205 lang=cpp
 *
 * [205] Isomorphic Strings
 */

// @lc code=start
class Solution
{
private:
    bool check(const string &s, const string &t)
    {
        char m[128] = { 0 }; // t中的字母需要被替换成哪个字母
        for(int i = 0; i < t.length(); i++)
            if(m[t[i]] == 0)
                m[t[i]] = s[i];
            else if(s[i] != m[t[i]])
                return false;
        return true;
    }

public:
    bool isIsomorphic(const string &s, const string &t)
    {
        return check(s, t) && check(t, s);
    }
};
// @lc code=end
