/*
 * @lc app=leetcode id=140 lang=cpp
 *
 * [140] Word Break II
 */

// @lc code=start
class Solution
{
private:
    unordered_map<int, vector<string>> cache;

    vector<string> wordBreak(const string &s, const unordered_map<int, unordered_set<string>> &words,
                   int start)
    {
        if(start >= s.length())
            return {""};
        if(cache.count(start))
            return cache[start];
        vector<string> result;
        for(const auto &wordset: words)
            if(start + wordset.first <= s.length())
            {
                string word = s.substr(start, wordset.first);
                if(wordset.second.count(word))
                    for(const string &tail: wordBreak(s, words, start + word.length()))
                        if(!tail.empty())
                            result.push_back(word + " " + tail);
                        else
                            result.push_back(word);
            }
        return cache[start] = result;
    }

public:
    vector<string> wordBreak(const string &s, vector<string>& wordDict)
    {
        unordered_map<int, unordered_set<string>> words;
        for(const string &word: wordDict)
            words[word.length()].insert(word);
        return wordBreak(s, words, 0);
    }
};
// @lc code=end
