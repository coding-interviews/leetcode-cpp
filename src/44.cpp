/*
 * @lc app=leetcode id=44 lang=cpp
 *
 * [44] Wildcard Matching
 */

// @lc code=start
static int _ = []()
{
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);
	return 0;
}();

class Solution
{
public:
	bool isMatch(const string& s, const string& p)
	{
        vector<vector<bool>> dp(s.length() + 1, vector<bool>(p.length() + 1, false));
		dp.front().front() = true;
        for (int j = 1; j <= p.length(); j++)
            dp[0][j] = dp[0][j - 1] && p[j - 1] == '*';
		for (int i = 1; i <= s.length(); i++)
			for (int j = 1; j <= p.length(); j++)
				if (p[j - 1] == '*')
					dp[i][j] = dp[i - 1][j] || dp[i][j - 1];
                else if(p[j - 1] == '?')
                    dp[i][j] = dp[i - 1][j - 1];
				else
					dp[i][j] = dp[i - 1][j - 1] && s[i - 1] == p[j - 1];
		return dp.back().back();
	}
};
// @lc code=end
