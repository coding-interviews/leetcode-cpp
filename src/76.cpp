class Solution
{
public:
    string minWindow(string &s, string &t)
    {
        int source[128] = {0}, target[128] = {0};
        for (char ch : t)
            target[ch]++;
        int target_num = count_if(target, target + sizeof(target) / sizeof(int), [](int value) {
            return value > 0;
        });
        int source_num = 0;

        string minstr;
        int minlen = INT_MAX;
        for (int left = 0, right = 0; right < s.length(); right++)
        {
            if (target[s[right]])
            {
                source[s[right]]++;
                // 字母s[right]出现的次数恰好满足要求
                if (source[s[right]] == target[s[right]])
                    source_num++;
            }
            while (source_num >= target_num)
            {
                if (right - left + 1 < minlen)
                    minlen = right - left + 1, minstr = s.substr(left, minlen);
                if (target[s[left]])
                {
                    if (source[s[left]] == target[s[left]])
                        source_num--;
                    source[s[left]]--;
                }
                left++;
            }
        }
        return minstr;
    }
};
