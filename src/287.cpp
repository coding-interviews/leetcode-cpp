/*
 * @lc app=leetcode id=287 lang=cpp
 *
 * [287] Find the Duplicate Number
 */

// @lc code=start
class Solution
{
public:
    int findDuplicate(vector<int> &nums)
    {
        for (int i = 0; i < nums.size(); i++)
            while (nums[i] - 1 < nums.size() && i != nums[i] - 1)
                if (nums[i] != nums[nums[i] - 1])
                    swap(nums[i], nums[nums[i] - 1]);
                else
                    return nums[i];
        return -1;
    }
};

// 1 3 4 2 2
// 1 4 3 2 2
// 1 2 3 4 2
// 1 2 3 4 2
// @lc code=end
