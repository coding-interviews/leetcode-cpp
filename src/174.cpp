/*
 * @lc app=leetcode id=174 lang=cpp
 *
 * [174] Dungeon Game
 */

// @lc code=start
class Solution
{
public:
    int calculateMinimumHP(vector<vector<int>>& dungeon)
    {
        const int M = dungeon.size(), N  = dungeon.front().size();
        dungeon.back().back() = max(0, -dungeon.back().back());
        for(int i = M - 1; i >= 0; i--)
            for(int j = N - 1; j >= 0; j--)
                if(i != M - 1 || j != N - 1)
                    dungeon[i][j] = max(0, - dungeon[i][j] 
                                    + min(i < M - 1? dungeon[i + 1][j]: INT_MAX,
                                          j < N - 1? dungeon[i][j + 1]: INT_MAX));
        return dungeon.front().front() + 1;
    }
};
// @lc code=end
