/*
 * @lc app=leetcode id=121 lang=cpp
 *
 * [121] Best Time to Buy and Sell Stock
 */

// @lc code=start
class Solution
{
public:
    int maxProfit(vector<int> &prices)
    {
        int minPrice = INT_MAX, profit = 0;
        for (int price : prices)
        {
            profit = max(profit, price - minPrice);
            minPrice = min(minPrice, price);
        }
        return profit;
    }
};
// @lc code=end
