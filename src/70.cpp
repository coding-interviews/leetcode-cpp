/*
 * @lc app=leetcode id=70 lang=cpp
 *
 * [70] Climbing Stairs
 */

// @lc code=start
class Solution
{
public:
    int climbStairs(int n)
    {
        // dp[i] = dp[i - 1] + dp[i - 2]
        if(n == 1)
            return 1;
        else if(n == 2)
            return 2;
        int result = 0;
        for(int i = 3, dp_2 = 1, dp_1 = 2; i <= n; i++)
            result = dp_2 + dp_1, dp_2 = dp_1, dp_1 = result;
        return result;
    }
};
// @lc code=end
