/*
 * @lc app=leetcode id=518 lang=cpp
 *
 * [518] Coin Change 2
 */

// @lc code=start
class Solution
{
public:
    int change(int amount, vector<int> &coins)
    {
        vector<int> dp(amount + 1, 0);
        dp[0] = 1;
        for (int i = 1; i <= coins.size(); i++)
            for (int w = 1; w <= amount; w++)
                if (w >= coins[i - 1])
                    dp[w] += dp[w - coins[i - 1]];
        return dp.back();
    }
};
// @lc code=end
