/*
 * @lc app=leetcode id=38 lang=cpp
 *
 * [38] Count and Say
 */

// @lc code=start
class Solution
{
private:
    string countAndSay(const string &s)
    {
        string result;
        char prev = s.front();
        int count = 1;
        for(int i = 1; i < s.length(); i++)
            if(s[i] == prev)
                count++;
            else
            {
                result.push_back('0' + count);
                result.push_back(prev);
                prev = s[i], count = 1;
            }
        result.push_back('0' + count);
        result.push_back(prev);
        return result;
    }

public:
    string countAndSay(int n)
    {
        string result = "1";
        for(int i = 2; i <= n; i++)
            result = countAndSay(result);
        return result;
    }
};
// @lc code=end
