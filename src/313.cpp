/*
 * @lc app=leetcode id=313 lang=cpp
 *
 * [313] Super Ugly Number
 */

// @lc code=start
class Solution
{
public:
    int nthSuperUglyNumber(int n, vector<int>& primes)
    {
        vector<int> numbers(n, 1);
        vector<const int *> p(primes.size(), numbers.data());
        for(int i = 1; i < n; i++)
        {
            numbers[i] = INT_MAX;
            for(int j = 0; j < primes.size(); j++)
                numbers[i] = min(numbers[i], *p[j] * primes[j]);
            for(int j = 0; j < primes.size(); j++)
                if(numbers[i] == *p[j] * primes[j])
                    p[j]++;
        }
        return numbers.back();
    }
};
// @lc code=end
