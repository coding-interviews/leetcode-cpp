/*
 * @lc app=leetcode id=551 lang=cpp
 *
 * [551] Student Attendance Record I
 */

// @lc code=start
class Solution
{
public:
    bool checkRecord(string &s)
    {
        int absent = 0;
        for (int i = 0; i < s.length(); i++)
        {
            if (s[i] == 'A')
                absent++;
            if (absent > 1)
                return false;
            if (i > 1 && s[i - 2] == 'L' && s[i - 1] == 'L' && s[i] == 'L')
                return false;
        }
        return true;
    }
};
// @lc code=end
