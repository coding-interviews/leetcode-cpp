/*
 * @lc app=leetcode id=739 lang=cpp
 *
 * [739] Daily Temperatures
 */

// @lc code=start
class Solution
{
public:
    vector<int> dailyTemperatures(vector<int> &T)
    {
        // 从右向左维护一个单调递减栈
        vector<int> result;
        stack<int> s; // 存放的是下标
        for (int i = T.size() - 1; i >= 0; i--)
        {
            while (!s.empty() && T[i] >= T[s.top()])
                s.pop();
            if (!s.empty())
                result.push_back(s.top() - i);
            else
                result.push_back(0);
            s.push(i);
        }
        reverse(result.begin(), result.end());
        return result;
    }
};
// @lc code=end
