/*
 * @lc app=leetcode id=332 lang=cpp
 *
 * [332] Reconstruct Itinerary
 */

// @lc code=start
typedef priority_queue<string, vector<string>, greater<string>> Heap;

class Solution
{
private:
    void dfs(vector<string> &result, string &current, unordered_map<string, Heap> &graph)
    {
        while (graph.count(current) && !graph[current].empty())
        {
            string next = graph[current].top();
            graph[current].pop();
            dfs(result, next, graph);
        }
        result.push_back(current);
    }

public:
    vector<string> findItinerary(vector<vector<string>> &tickets)
    {
        unordered_map<string, Heap> graph;
        for (const vector<string> &ticket : tickets)
            graph[ticket[0]].push(ticket[1]);

        vector<string> result;
        string start = "JFK";
        dfs(result, start, graph);
        reverse(result.begin(), result.end());
        return result;
    }
};
// @lc code=end
