/*
 * @lc app=leetcode id=60 lang=cpp
 *
 * [60] Permutation Sequence
 */

// @lc code=start
class Solution
{
private:
    template <typename Collection>
    bool next_permutation(Collection &nums)
	{
		// 找出第一个左<右的位置
		int left = nums.size() - 2;
		while (nums[left] >= nums[left + 1])
		{
			left--;
			if (left < 0)
				return false;	// 已经是最后一个排列
		}
		// 从右往左找出第一个>left的位置
		int right = nums.size() - 1;
		while (left < right && nums[left] >= nums[right])
			right--;
		// 交换left和right
		swap(nums[left], nums[right]);
		// 逆序left以后的数字
		reverse(nums.begin() + left + 1, nums.end());
		return true;
	}

public:
    string getPermutation(int n, int k)
    {
        string s(n, '0');
        for(int i = 1; i <= n; i++)
            s[i - 1] += i;
        for(int i = 1; i < k; i++)
            next_permutation(s);
        return s;
    }
};
// @lc code=end
