/*
 * @lc app=leetcode id=37 lang=cpp
 *
 * [37] Sudoku Solver
 */

// @lc code=start
class Solution
{
private:
    pair<int, int> nextPosition(vector<vector<char>>& board, int i, int j)
    {
        for (int y = j + 1; y < 9; y++)
            if (board[i][y] == '.')
                return { i, y };
        for (int x = i + 1; x < 9; x++)
            for (int y = 0; y < 9; y++)
                if (board[x][y] == '.')
                    return { x, y };
        return { -1, -1 };
    }

    bool solveSudoku(vector<vector<char>>& board,
        bool rows[9][10], bool cols[9][10], bool blocks[9][10],
        int i, int j)
    {
        for (int num = 1; num <= 9; num++)
        {
            if(rows[i][num] || cols[j][num] || blocks[i / 3 * 3 + j / 3][num])
                continue;
            board[i][j] = '0' + num;
            rows[i][num] = cols[j][num] = blocks[i / 3 * 3 + j / 3][num] = true;
            pair<int, int> next = nextPosition(board, i, j);
            if (next.first == -1 || solveSudoku(board, rows, cols, blocks, next.first, next.second))
                return true;
            blocks[i / 3 * 3 + j / 3][num] = cols[j][num] = rows[i][num] = false;
            board[i][j] = '.';
        }
        return false;
    }

public:
    void solveSudoku(vector<vector<char>>& board)
    {
        bool rows[9][10] = {{false}}, cols[9][10] = {{false}}, blocks[9][10] = {{false}};
        // 把现有数字添加进来
        for(int i = 0; i < 9; i++)
            for(int j = 0; j < 9; j++)
                if(board[i][j] != '.')
                {
                    int num = board[i][j] - '0';
                    rows[i][num] = cols[j][num] = blocks[i / 3 * 3 + j / 3][num] = true;
                }
        pair<int, int> next = nextPosition(board, 0, -1);
        if(next.first != -1)
            solveSudoku(board, rows, cols, blocks, next.first, next.second);
    }
};

// @lc code=end
