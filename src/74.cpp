class Solution
{
public:
    bool searchMatrix(vector<vector<int>> &matrix, int target)
    {
        if (matrix.empty() || matrix.front().empty())
            return false;
        int M = matrix.size(), N = matrix.front().size();
        // O(logM + logN)
        int left = 0, right = M * N - 1;
        while (left + 1 < right)
        {
            int mid = left + (right - left) / 2;
            if (matrix[mid / N][mid % N] < target)
                left = mid;
            else if (matrix[mid / N][mid % N] > target)
                right = mid;
            else
                return true;
        }
        return matrix[left / N][left % N] == target || matrix[right / N][right % N] == target;
    }
};
