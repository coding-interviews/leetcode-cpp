class Solution
{
public:
    int maxArea(vector<int> &height)
    {
        int maxwater = 0;
        for (int left = 0, right = height.size() - 1; left < right;)
        {
            maxwater = max(maxwater, (right - left) * min(height[left], height[right]));
            if (height[left] < height[right])
                left++;
            else
                right--;
        }
        return maxwater;
    }
};