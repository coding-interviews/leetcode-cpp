/*
 * @lc app=leetcode id=668 lang=cpp
 *
 * [668] Kth Smallest Number in Multiplication Table
 */

// @lc code=start
class Solution
{
private:
    int numSmallerOrEqual(int m, int n, int num)
    {
        int count = 0;
        for (int i = 1; i <= m; i++)
            count += min(num / i, n);
        return count;
    }

public:
    int findKthNumber(int m, int n, int k)
    {
        int left = 1, right = m * n;
        while (left + 1 < right)
        {
            int mid = left + (right - left) / 2;
            if (numSmallerOrEqual(m, n, mid) < k)
                left = mid;
            else
                right = mid;
        }
        return numSmallerOrEqual(m, n, left) >= k ? left : right;
    }
};
// @lc code=end
