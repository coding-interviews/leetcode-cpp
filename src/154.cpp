class Solution
{
public:
    int findMin(vector<int> &nums)
    {
        int left = 0, right = nums.size() - 1;
        while (left < right && nums[left] == nums[right])
            right--;
        int back = nums[right];
        while (left + 1 < right)
        {
            int mid = left + (right - left) / 2;
            if (nums[mid] > back)
                left = mid;
            else
                right = mid;
        }
        return min(nums[left], nums[right]);
    }
};