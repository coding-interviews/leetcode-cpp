/*
 * @lc app=leetcode id=315 lang=cpp
 *
 * [315] Count of Smaller Numbers After Self
 */

// @lc code=start
class Solution
{
private:
    struct BIT
    {
        vector<int> bit;

        int lowbit(int x)
        {
            return x & (-x);
        }

        BIT(int range)
        {
            bit.resize(range + 1);
        }

        void update(int index, int delta)
        {
            for (int i = index + 1; i < bit.size(); i += lowbit(i))
                bit[i] += delta;
        }

        int prefixSum(int index)
        {
            int sum = 0;
            for (int i = index + 1; i >= 1; i -= lowbit(i))
                sum += bit[i];
            return sum;
        }
    };

public:
    vector<int> countSmaller(vector<int> &nums)
    {
        if (nums.empty())
            return {};
        int minval = *min_element(nums.begin(), nums.end());
        int maxval = *max_element(nums.begin(), nums.end());

        vector<int> result(nums.size());
        BIT bit(maxval - minval + 1);
        for (int i = nums.size() - 1; i >= 0; i--)
        {
            int num = nums[i] - minval + 1;
            result[i] = bit.prefixSum(num - 1);
            bit.update(num, 1);
        }
        return result;
    }
};

// @lc code=end
