/*
 * @lc app=leetcode id=228 lang=cpp
 *
 * [228] Summary Ranges
 */

// @lc code=start
class Solution
{
public:
    vector<string> summaryRanges(vector<int> &nums)
    {
        vector<pair<int, int>> ranges;
        for (int i = 0; i < nums.size(); i++)
            if (i > 0 && nums[i] == nums[i - 1] + 1)
                ranges.back().second = nums[i];
            else
                ranges.push_back({nums[i], nums[i]});
        vector<string> result;
        for (const pair<int, int> &range : ranges)
            if (range.first == range.second)
                result.push_back(to_string(range.first));
            else
                result.push_back(to_string(range.first) + "->" + to_string(range.second));
        return result;
    }
};
// @lc code=end
