/*
 * @lc app=leetcode id=131 lang=cpp
 *
 * [131] Palindrome Partitioning
 */

// @lc code=start
class Solution
{
private:
    vector<vector<bool>> palindrome;

    void partition(vector<vector<string>> &result, vector<string> &path, const string &s, int left)
    {
        if (left >= s.length())
            result.push_back(path);
        for (int right = left; right < s.length(); right++)
            if (palindrome[left][right])
            {
                path.push_back(s.substr(left, right - left + 1));
                partition(result, path, s, right + 1);
                path.pop_back();
            }
    }

public:
    vector<vector<string>> partition(string s)
    {
        if (s.empty())
            return {};
        palindrome.resize(s.length(), vector<bool>(s.length(), true));
        for (int i = s.length() - 2; i >= 0; i--)
            for (int j = i + 1; j < s.length(); j++)
                palindrome[i][j] = s[i] == s[j] && palindrome[i + 1][j - 1];

        vector<vector<string>> result;
        vector<string> path;
        partition(result, path, s, 0);
        return result;
    }
};
// @lc code=end
