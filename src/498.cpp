/*
 * @lc app=leetcode id=498 lang=cpp
 *
 * [498] Diagonal Traverse
 */

// @lc code=start
class Solution
{
public:
    vector<int> findDiagonalOrder(vector<vector<int>> &matrix)
    {
        if (matrix.empty() || matrix.front().empty())
            return {};
        int M = matrix.size(), N = matrix.front().size();
        vector<int> result;
        result.reserve(M * N);
        for (int i = 0, j = 0, dir = 0; i < M && j < N;)
        {
            result.push_back(matrix[i][j]);
            if (dir == 0)
            {
                if (j + 1 >= N)
                    i++, dir = !dir;
                else if (i == 0)
                    j++, dir = !dir;
                else
                    i--, j++;
            }
            else
            {
                if (i + 1 >= M)
                    j++, dir = !dir;
                else if (j == 0)
                    i++, dir = !dir;
                else
                    i++, j--;
            }
        }
        return result;
    }
};
// @lc code=end
