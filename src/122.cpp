/*
 * @lc app=leetcode id=122 lang=cpp
 *
 * [122] Best Time to Buy and Sell Stock II
 */

// @lc code=start
class Solution
{
public:
    int maxProfit(vector<int>& prices)
    {
        int prev = INT_MAX, profit = 0;
        for(int price: prices)
        {
            profit += max(0, price - prev);
            prev = price;
        }
        return profit;
    }
};
// @lc code=end
