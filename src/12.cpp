class Solution
{
public:
    string intToRoman(int num)
    {
        map<int, string> dict;
        dict[1] = "I";
        dict[4] = "IV";
        dict[5] = "V";
        dict[9] = "IX";
        dict[10] = "X";
        dict[40] = "XL";
        dict[50] = "L";
        dict[90] = "XC";
        dict[100] = "C";
        dict[400] = "CD";
        dict[500] = "D";
        dict[900] = "CM";
        dict[1000] = "M";
        string result;
        for (auto it = dict.rbegin(); it != dict.rend();)
            if (num >= it->first)
                result += it->second, num -= it->first;
            else
                ++it;
        return result;
    }
};