/*
 * @lc app=leetcode id=216 lang=cpp
 *
 * [216] Combination Sum III
 */

// @lc code=start
class Solution
{
private:
    void combinationSum3(vector<vector<int>> &result, vector<int> &path, int k, int n)
    {
        if (k == 0)
        {
            if (n == 0)
                result.push_back(path);
            return;
        }
        for (int next = path.empty() ? 1 : path.back() + 1; next <= 9 && n - next >= 0; next++)
        {
            path.push_back(next);
            combinationSum3(result, path, k - 1, n - next);
            path.pop_back();
        }
    }

public:
    vector<vector<int>> combinationSum3(int k, int n)
    {
        vector<vector<int>> result;
        vector<int> path;
        combinationSum3(result, path, k, n);
        return result;
    }
};

// @lc code=end
