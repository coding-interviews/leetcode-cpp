/*
 * @lc app=leetcode id=409 lang=cpp
 *
 * [409] Longest Palindrome
 */

// @lc code=start
class Solution
{
public:
    int longestPalindrome(const string &s)
    {
        int counter[128] = { 0 };
        for(char ch: s)
            counter[ch]++;
        int odd = 0, even = 0;
        for(int i = 0; i < sizeof(counter) / sizeof(int); i++)
            if(counter[i] % 2)
                odd = 1, even += counter[i] - 1;
            else if(counter[i] != 0 && counter[i] % 2 == 0)
                even += counter[i];
        return odd + even;
    }
};
// @lc code=end
