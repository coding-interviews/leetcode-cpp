class Solution
{
public:
    int longestValidParentheses(string &s)
    {
        if (s.empty())
            return 0;

        vector<int> dp(s.length(), 0);
        stack<int> st; // 存放左括号的坐标
        for (int i = 0; i < s.length(); i++)
        {
            if (s[i] == '(')
                st.push(i);
            else if (!st.empty())
            {
                dp[i] = st.top() > 0 ? dp[st.top() - 1] + i - st.top() + 1 : i - st.top() + 1;
                st.pop();
            }
        }
        return *max_element(dp.begin(), dp.end());
    }
};
