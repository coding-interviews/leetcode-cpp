/*
 * @lc app=leetcode id=377 lang=cpp
 *
 * [377] Combination Sum IV
 */

// @lc code=start
class Solution
{
private:
    unordered_map<int, int> cache;

public:
    int combinationSum4(vector<int>& nums, int target)
    {
        if(target < 0)
            return 0;
        if(target == 0)
            return 1;
        if(cache.count(target))
            return cache[target];
        int ways = 0;
        for(int num: nums)
            ways += combinationSum4(nums, target - num);
        return cache[target] = ways;
    }
};
// @lc code=end
