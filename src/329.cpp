/*
 * @lc app=leetcode id=329 lang=cpp
 *
 * [329] Longest Increasing Path in a Matrix
 */

// @lc code=start
class Solution
{
private:
    int dfs(vector<vector<int>> &matrix, vector<vector<int>> &cache, int i, int j, int prev)
    {
        int M = matrix.size(), N = matrix.front().size();
        if (i < 0 || i >= M || j < 0 || j >= N || prev >= matrix[i][j])
            return 0;
        if (cache[i][j])
            return cache[i][j];
        int maxlen = 0;
        maxlen = max(maxlen, dfs(matrix, cache, i - 1, j, matrix[i][j]));
        maxlen = max(maxlen, dfs(matrix, cache, i + 1, j, matrix[i][j]));
        maxlen = max(maxlen, dfs(matrix, cache, i, j - 1, matrix[i][j]));
        maxlen = max(maxlen, dfs(matrix, cache, i, j + 1, matrix[i][j]));
        return cache[i][j] = 1 + maxlen;
    }

public:
    int longestIncreasingPath(vector<vector<int>> &matrix)
    {
        if (matrix.empty() || matrix.front().empty())
            return 0;
        int M = matrix.size(), N = matrix.front().size();
        vector<vector<int>> cache(M, vector<int>(N, 0));
        int maxlen = 0;
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                maxlen = max(maxlen, dfs(matrix, cache, i, j, INT_MIN));
        return maxlen;
    }
};

// @lc code=end
