/*
 * @lc app=leetcode id=301 lang=cpp
 *
 * [301] Remove Invalid Parentheses
 */

// @lc code=start
class Solution
{
private:
	void numberOfInvalidParentheses(const string& s, int& left, int& right)
	{
		left = 0, right = 0;
		for (char ch : s)
			if (ch == '(')
				left++;
			else if (ch == ')')
			{
				if (left > 0)
					left--;
				else
					right++;
			}
	}

	void removeInvalidParentheses(const char* s, int validLeft, int invalidLeft, int invalidRight,
		string& path, unordered_set<string>& result)
	{
		if (*s == '\0')
		{
			if (invalidLeft == 0 && invalidRight == 0)
				result.insert(path);
			return;
		}
		if ((*s == '(' && invalidLeft > 0) || (*s == ')' && invalidRight > 0))
			removeInvalidParentheses(s + 1, validLeft, invalidLeft - (*s == '('), invalidRight - (*s == ')'), path, result);
		path.push_back(*s);
		if (*s != '(' && *s != ')')
			removeInvalidParentheses(s + 1, validLeft, invalidLeft, invalidRight, path, result);
		else if (*s == '(')
			removeInvalidParentheses(s + 1, validLeft + 1, invalidLeft, invalidRight, path, result);
		else if (validLeft > 0)
			removeInvalidParentheses(s + 1, validLeft - 1, invalidLeft, invalidRight, path, result);
		path.pop_back();
	}

public:
	vector<string> removeInvalidParentheses(const string& s)
	{
		int left = 0, right = 0;
		numberOfInvalidParentheses(s, left, right);
		unordered_set<string> result_set;
		string path;
		removeInvalidParentheses(s.c_str(), 0, left, right, path, result_set);
		vector<string> result(result_set.size());
		copy(result_set.begin(), result_set.end(), result.begin());
		return result;
	}
};
// @lc code=end
