/*
 * @lc app=leetcode id=119 lang=cpp
 *
 * [119] Pascal's Triangle II
 */

// @lc code=start
class Solution
{
public:
    vector<int> getRow(int rowIndex)
    {
        vector<int> result = { 1 };
        for(int i = 1; i <= rowIndex; i++)
        {
            result.push_back(0);
            for(int j = 0, left = 0; j <= i; j++)
            {
                int prev = result[j];
                result[j] += left;
                left = prev;
            }
        }
        return result;
    }
};
// @lc code=end
