/*
 * @lc app=leetcode id=28 lang=cpp
 *
 * [28] Implement strStr()
 */

// @lc code=start
class Solution
{
public:
    int strStr(string haystack, string needle)
    {
        int pos = haystack.find(needle);
        return pos != string::npos? pos: -1;
    }
};
// @lc code=end
