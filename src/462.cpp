/*
 * @lc app=leetcode id=462 lang=cpp
 *
 * [462] Minimum Moves to Equal Array Elements II
 */

// @lc code=start
class Solution
{
private:
    long long numMoves(vector<int> &nums, long long target)
    {
        long long result = 0;
        for (long long num : nums)
            result += abs(num - target);
        return result;
    }

public:
    int minMoves2(vector<int> &nums)
    {
        if (nums.empty())
            return 0;
        nth_element(nums.begin(), nums.begin() + nums.size() / 2, nums.end());
        return numMoves(nums, nums[nums.size() / 2]);
    }
};

// @lc code=end
