class Solution
{
public:
    vector<int> spiralOrder(vector<vector<int>> &matrix)
    {
        if (matrix.empty() || matrix.front().empty())
            return {};
        int M = matrix.size(), N = matrix.front().size();
        vector<int> result;
        result.reserve(M * N);
        for (int level = 0; level <= min((M - 1) / 2, (N - 1) / 2); level++)
        {
            for (int j = level; j < N - level; j++)
                result.push_back(matrix[level][j]);
            if (level + 1 >= M - level)
                return result; // 下面的循环不满足初始条件时，后面的也不用做了
            for (int i = level + 1; i < M - level; i++)
                result.push_back(matrix[i][N - level - 1]);
            if (N - level - 2 < level)
                return result; // 下面的循环不满足初始条件时，后面的也不用做了
            for (int j = N - level - 2; j >= level; j--)
                result.push_back(matrix[M - level - 1][j]);
            for (int i = M - level - 2; i > level; i--)
                result.push_back(matrix[i][level]);
        }
        return result;
    }
};
