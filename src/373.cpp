class Solution
{
public:
    vector<vector<int>> kSmallestPairs(vector<int> &nums1, vector<int> &nums2, int k)
    {
        if (nums1.empty() || nums2.empty())
            return {};
        multimap<int, pair<int, int>> heap;
        heap.insert({nums1[0] + nums2[0], {0, 0}});
        vector<vector<int>> result;
        while (!heap.empty() && result.size() < k)
        {
            int i = heap.begin()->second.first, j = heap.begin()->second.second;
            heap.erase(heap.begin());
            result.push_back({nums1[i], nums2[j]});
            if (j + 1 < nums2.size())
                heap.insert({nums1[i] + nums2[j + 1], {i, j + 1}});
            if (j == 0 && i + 1 < nums1.size())
                heap.insert({nums1[i + 1] + nums2[j], {i + 1, j}});
        }
        return result;
    }
};