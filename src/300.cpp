/*
 * @lc app=leetcode id=300 lang=cpp
 *
 * [300] Longest Increasing Subsequence
 */

// @lc code=start
class Solution
{
public:
    int lengthOfLIS(vector<int> &nums)
    {
        vector<int> dp(nums.size(), INT_MAX);
        for (int num : nums)
            *lower_bound(dp.begin(), dp.end(), num) = num;
        return lower_bound(dp.begin(), dp.end(), INT_MAX) - dp.begin();
    }
};

// @lc code=end
