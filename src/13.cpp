/*
 * @lc app=leetcode id=13 lang=cpp
 *
 * [13] Roman to Integer
 */

// @lc code=start
class Solution
{
public:
	int romanToInt(const string& s)
	{
		int result = 0;
		for (int i = 0; i < s.length(); i++)
			switch (s[i])
			{
			case 'I':
				result += i + 1 < s.length() && (s[i + 1] == 'V' || s[i + 1] == 'X') ? -1 : 1;
				break;
			case 'V':
				result += 5;
				break;
			case 'X':
				result += i + 1 < s.length() && (s[i + 1] == 'L' || s[i + 1] == 'C') ? -10 : 10;
				break;
			case 'L':
				result += 50;
				break;
			case 'C':
				result += i + 1 < s.length() && (s[i + 1] == 'D' || s[i + 1] == 'M') ? -100 : 100;
				break;
			case 'D':
				result += 500;
				break;
			case 'M':
				result += 1000;
				break;
			}
		return result;
	}
};
// @lc code=end
