/*
 * @lc app=leetcode id=172 lang=cpp
 *
 * [172] Factorial Trailing Zeroes
 */

// @lc code=start
class Solution
{
public:
    int trailingZeroes(int n)
    {
        int result = 0;
        while(n)
        {
            result += n / 5;
            n /= 5;
        }
        return result;
    }
};
// @lc code=end
