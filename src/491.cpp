/*
 * @lc app=leetcode id=491 lang=cpp
 *
 * [491] Increasing Subsequences
 */

// @lc code=start
class Solution
{
private:
    struct Hash
    {
        size_t operator()(const vector<int>& v) const
        {
            hash<int> h;
            size_t h1 = 0, h2 = 0;
            for (int i = 0; i < min(15, (int)v.size()); i++)
                h1 = (h1 << 8) | h(v[i]);
            for (int i = 15; i < v.size(); i++)
                h2 = (h2 << 8) | h(v[i]);
            return h1 ^ h2;
        }
    };

    struct Compare
    {
        bool operator()(const vector<int>& v1, const vector<int>& v2) const
        {
            if (v1.size() != v2.size())
                return false;
            for (int i = 0; i < v1.size(); i++)
                if (v1[i] != v2[i])
                    return false;
            return true;
        }
    };

    typedef unordered_set<vector<int>, Hash, Compare> HashSet;

    void subsets(HashSet& result, vector<int>& path, vector<int>& nums, int i)
    {
        if (path.size() > 1)
            result.insert(path);
        for (int next = i; next < nums.size(); next++)
        {
            if (!path.empty() && path.back() > nums[next])
                continue;
            path.push_back(nums[next]);
            subsets(result, path, nums, next + 1);
            path.pop_back();
        }
    }
public:
    vector<vector<int>> findSubsequences(vector<int>& nums)
    {
        HashSet sequences;
        vector<int> path;
        subsets(sequences, path, nums, 0);
        return vector<vector<int>>(sequences.begin(), sequences.end());
    }
};
// @lc code=end
