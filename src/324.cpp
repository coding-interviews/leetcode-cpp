/*
 * @lc app=leetcode id=324 lang=cpp
 *
 * [324] Wiggle Sort II
 */

// @lc code=start
class Solution
{
public:
    void wiggleSort(vector<int> &nums)
    {
        vector<int> sorted = nums;
        sort(sorted.begin(), sorted.end());
        for (int i = 0, left = (nums.size() - 1) / 2, right = nums.size() - 1; i < nums.size(); i++)
            if (i % 2)
                nums[i] = sorted[right--];
            else
                nums[i] = sorted[left--];
    }
};
// @lc code=end
