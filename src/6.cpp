class Solution
{
public:
    string convert(string &s, int numRows)
    {
        if (numRows <= 1)
            return s;
        vector<string> matrix(numRows);
        int i = 0;
        bool down = true;
        for (char ch : s)
        {
            matrix[i].push_back(ch);
            if (down)
                if (i == numRows - 1)
                    i--, down = !down;
                else
                    i++;
            else if (i == 0)
                i++, down = !down;
            else
                i--;
        }
        string result;
        for (string &row : matrix)
            result += row;
        return result;
    }
};
