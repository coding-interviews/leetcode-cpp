/*
 * @lc app=leetcode id=283 lang=cpp
 *
 * [283] Move Zeroes
 */

// @lc code=start
class Solution
{
public:
    void moveZeroes(vector<int>& nums)
    {
        int zeros = count(nums.begin(), nums.end(), 0);
        for(int left = 0, right = 0; left < nums.size() - zeros; left++, right++)
        {
            while(nums[right] == 0)
                right++;
            nums[left] = nums[right];
        }
        fill(nums.end() - zeros, nums.end(), 0);
    }
};
// @lc code=end
