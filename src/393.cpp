/*
 * @lc app=leetcode id=393 lang=cpp
 *
 * [393] UTF-8 Validation
 */

// @lc code=start
class Solution
{
public:
    bool validUtf8(vector<int> &data)
    {
        int N = data.size(), i = 0;
        while (i < N)
        {
            // 检查第一个字节
            int n = 0; // 开头1的个数
            for (unsigned int test = 0b10000000; data[i] & test; test >>= 1)
                n++;
            if (n == 1 || n > 4)
                return false; // 最高位只可能是0, 110, 1110, 11110
            // 检查剩余n个字节
            i++;
            for (n--; n > 0; n--) // 若第一个字节有n个1，则需要遍历后续n-1个字节
            {
                if (i >= N || !((data[i] >> 6) & 0b10))
                    return false;
                i++;
            }
        }
        return true;
    }
};
// @lc code=end
