/*
 * @lc app=leetcode id=306 lang=cpp
 *
 * [306] Additive Number
 */

// @lc code=start
class Solution
{
private:
	bool isAdditiveNumber(const string& num, uint64_t num1, uint64_t num2, int start)
	{
		while (start < num.length())
		{
			ostringstream ss;
			uint64_t num3 = num1 + num2;
			ss << num3;
			string str = ss.str();
			ss.clear();
			if (start + str.length() > num.length() || num.compare(start, str.length(), str) != 0)
				return false;
			start += str.length();
			num1 = num2, num2 = num3;
		}
		return true;
	}

public:
	bool isAdditiveNumber(const string& num)
	{
		for (int n1 = 1; n1 <= num.length(); n1++)
		{
			uint64_t num1 = stoull(num.substr(0, n1));
			// num1不能以0开头
			if (num[0] == '0' && num1 != 0)
				continue;
			for (int n2 = 1; n1 + n2 < num.length(); n2++)
			{
				// num2不能以0开头
				uint64_t num2 = stoull(num.substr(n1, n2));
				if (num[n1] == '0' && num2 != 0)
					continue;
				if (isAdditiveNumber(num, num1, num2, n1 + n2))
					return true;
			}
		}
		return false;
	}
};
// @lc code=end
