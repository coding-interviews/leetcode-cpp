/*
 * @lc app=leetcode id=65 lang=cpp
 *
 * [65] Valid Number
 */

// @lc code=start
const regex re(R"(^\s*[+|-]?(\d+\.?\d*|\d*\.?\d+)(e[+|-]?\d+)?\s*$)");

class Solution
{
public:
	bool isNumber(const string &s)
	{
		return regex_match(s, re);
	}
};
// @lc code=end
