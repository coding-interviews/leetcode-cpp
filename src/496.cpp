/*
 * @lc app=leetcode id=496 lang=cpp
 *
 * [496] Next Greater Element I
 */

// @lc code=start
class Solution
{
public:
    vector<int> nextGreaterElement(vector<int>& nums1, vector<int>& nums2)
    {
        unordered_map<int, int> pos;
        for(int i = 0; i < nums2.size(); i++)
            pos[nums2[i]] = i;
        vector<int> result;
        for(int num: nums1)
        {
            int i = pos[num] + 1;
            while(i < nums2.size() && nums2[i] <= num)
                i++;
            result.push_back(i < nums2.size()? nums2[i]: -1);
        }
        return result;
    }
};
// @lc code=end
