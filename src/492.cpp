/*
 * @lc app=leetcode id=492 lang=cpp
 *
 * [492] Construct the Rectangle
 */

// @lc code=start
class Solution
{
public:
    vector<int> constructRectangle(int area)
    {
        for(int width = sqrt(area); width > 0; width--)
            if(area % width == 0)
                return {area / width, width};
        return {-1, -1};
    }
};
// @lc code=end
