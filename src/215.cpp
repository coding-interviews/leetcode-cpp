/*
 * @lc app=leetcode id=215 lang=cpp
 *
 * [215] Kth Largest Element in an Array
 */

// @lc code=start
class Solution
{
private:
    int quickSelect(vector<int> &nums, int begin, int end, int k)
    {
        if (begin >= end)
            return nums[begin];
        int left = begin, right = end, pivot = nums[left + (right - left) / 2];
        while (left <= right)
        {
            while (left <= right && nums[left] > pivot)
                left++;
            while (left <= right && nums[right] < pivot)
                right--;
            if (left <= right)
                swap(nums[left++], nums[right--]);
        }
        if (left <= k - 1)
            return quickSelect(nums, left, end, k);
        else if (right >= k - 1)
            return quickSelect(nums, begin, right, k);
        else
            return nums[k - 1];
    }

public:
    int findKthLargest(vector<int> &nums, int k)
    {
        return quickSelect(nums, 0, nums.size() - 1, k);
    }
};
// @lc code=end
