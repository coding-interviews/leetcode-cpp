/*
 * @lc app=leetcode id=419 lang=cpp
 *
 * [419] Battleships in a Board
 */

// @lc code=start
class Solution
{
private:
    bool isHorizontalShip(const vector<vector<char>> &board, int i, int j)
    {
        int M = board.size(), N = board.front().size();
        return (i == 0 || board[i - 1][j] != 'X') &&
               (j == 0 || board[i][j - 1] != 'X') &&
               (i == M - 1 || board[i + 1][j] != 'X') &&
               (j < N - 1 && board[i][j + 1] == 'X');
    }

    bool isVerticalShip(const vector<vector<char>> &board, int i, int j)
    {
        int M = board.size(), N = board.front().size();
        return (i == 0 || board[i - 1][j] != 'X') &&
               (j == 0 || board[i][j - 1] != 'X') &&
               (i < M - 1 && board[i + 1][j] == 'X') &&
               (j == N - 1 || board[i][j + 1] != 'X');
    }

    bool isSingleShip(const vector<vector<char>> &board, int i, int j)
    {
        int M = board.size(), N = board.front().size();
        return (i == 0 || board[i - 1][j] != 'X') &&
               (j == 0 || board[i][j - 1] != 'X') &&
               (i == M - 1 || board[i + 1][j] != 'X') &&
               (j == N - 1 || board[i][j + 1] != 'X');
    }

public:
    int countBattleships(vector<vector<char>>& board)
    {
        if(board.empty())
            return 0;
        int count = 0;
        for(int i = 0; i < board.size(); i++)
            for(int j = 0; j < board[i].size(); j++)
                count += board[i][j] == 'X' &&
                         (isHorizontalShip(board, i, j) ||
                          isVerticalShip(board, i, j) ||
                          isSingleShip(board, i, j));
        return count;
    }
};
// @lc code=end
