class Solution
{
private:
    double kthElement(vector<int> &nums1, vector<int> &nums2, int k, int start1, int start2)
    {
        if (start1 >= nums1.size())
            return nums2[start2 + k - 1];
        if (start2 >= nums2.size())
            return nums1[start1 + k - 1];
        if (k == 1)
            return min(nums1[start1], nums2[start2]);
        double half1 = start1 + k / 2 - 1 < nums1.size() ? nums1[start1 + k / 2 - 1] : INT_MAX;
        double half2 = start2 + k / 2 - 1 < nums2.size() ? nums2[start2 + k / 2 - 1] : INT_MAX;
        if (half1 < half2)
            return kthElement(nums1, nums2, k - k / 2, start1 + k / 2, start2);
        else
            return kthElement(nums1, nums2, k - k / 2, start1, start2 + k / 2);
    }

public:
    double findMedianSortedArrays(vector<int> &nums1, vector<int> &nums2)
    {
        int M = nums1.size(), N = nums2.size();
        return (kthElement(nums1, nums2, (M + N) / 2 + 1, 0, 0) +
                kthElement(nums1, nums2, (M + N - 1) / 2 + 1, 0, 0)) /
               2;
    }
};