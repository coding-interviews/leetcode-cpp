/*
 * @lc app=leetcode id=79 lang=cpp
 *
 * [79] Word Search
 */

// @lc code=start
class Solution
{
private:
    bool exist(vector<vector<char>> &board, int i, int j, const char *str)
    {
        if(*str == '\0')
            return true;
        int M = board.size(), N = board.front().size();
        if(i < 0 || i >= M || j < 0 || j >= N || board[i][j] == '\0' || board[i][j] != *str)
            return false;
        char backup = board[i][j];
        board[i][j] = '\0';
        bool found = exist(board, i - 1, j, str + 1) ||
                     exist(board, i + 1, j, str + 1) ||
                     exist(board, i, j - 1, str + 1) ||
                     exist(board, i, j + 1, str + 1);
        board[i][j] = backup;
        return found;
    }

public:
    bool exist(vector<vector<char>>& board, string &word)
    {
        if(board.empty() || board.front().empty())
            return false;
        int M = board.size(), N = board.front().size();
        for(int i = 0; i < M; i++)
            for(int j = 0; j < N; j++)
                if(exist(board, i, j, word.c_str()))
                    return true;
        return false;
    }
};
// @lc code=end
