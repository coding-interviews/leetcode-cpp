/*
 * @lc app=leetcode id=209 lang=cpp
 *
 * [209] Minimum Size Subarray Sum
 */

// @lc code=start
class Solution
{
public:
    int minSubArrayLen(int s, vector<int> &nums)
    {
        int sum = 0, minlen = INT_MAX;
        for (int left = 0, right = 0; right < nums.size(); right++)
        {
            sum += nums[right];
            while (sum >= s)
            {
                minlen = min(minlen, right - left + 1);
                sum -= nums[left++];
            }
        }
        return minlen < INT_MAX ? minlen : 0;
    }
};
// @lc code=end
