/*
 * @lc app=leetcode id=14 lang=cpp
 *
 * [14] Longest Common Prefix
 */

// @lc code=start
class Solution
{
public:
    string longestCommonPrefix(vector<string>& strs)
    {
        if(strs.empty())
            return "";
        string prefix;
        vector<const char *> pstrs;
        for(const string &str: strs)
            pstrs.push_back(str.c_str());
        while(true)
        {
            if(*pstrs[0] == '\0')
                return prefix;
            for(int i = 1; i < strs.size(); i++)
                if(*pstrs[i] == '\0' || *pstrs[i - 1]++ != *pstrs[i])
                    return prefix;
            prefix.push_back(*pstrs.back()++);
        }
        return prefix;
    }
};
// @lc code=end
