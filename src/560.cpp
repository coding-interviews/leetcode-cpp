/*
 * @lc app=leetcode id=560 lang=cpp
 *
 * [560] Subarray Sum Equals K
 */

// @lc code=start
class Solution
{
public:
    int subarraySum(vector<int> &nums, int k)
    {
        partial_sum(nums.begin(), nums.end(), nums.begin());
        unordered_map<int, int> dict;
        dict[0] = 1; // 不要忘了这一步
        int result = 0;
        for (int num : nums)
        {
            result += dict[num - k];
            dict[num]++;
        }
        return result;
    }
};

// @lc code=end
