/*
 * @lc app=leetcode id=400 lang=cpp
 *
 * [400] Nth Digit
 */

// @lc code=start
class Solution {
public:
    int findNthDigit(int n)
    {
        // 0, 9, 9+90*2, 9+90*2+900*3, 9+90*2+900*3+9000*4, ...
        int table[9] = {0};
        for(int i = 1, f = 9; i < sizeof(table) / sizeof(int); i++, f *= 10)
            table[i] = table[i - 1] + f * i;

        // 查表确定这是一个几位数
        int digits = lower_bound(table, table + sizeof(table) / sizeof(int), n) - table;
        
        // 在1后面添加digits-1个0，作为这一个数量级的第一个数字
        int base = 1;
        for(int i = 1; i < digits; i++)
            base *= 10;

        int diff = n - table[digits - 1] - 1;
        return to_string(base + diff / digits)[diff % digits] - '0';
    }
};
// @lc code=end
