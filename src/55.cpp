class Solution
{
public:
    bool canJump(vector<int> &nums)
    {
        // 贪心法背诵题
        int maxjump = 0;
        for (int i = 0; i < nums.size(); i++)
        {
            if (maxjump < i)
                return false;
            maxjump = max(maxjump, i + nums[i]);
        }
        return maxjump >= nums.size() - 1;
    }
};
