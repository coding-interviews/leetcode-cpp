/*
 * @lc app=leetcode id=376 lang=cpp
 *
 * [376] Wiggle Subsequence
 */

// @lc code=start
class Solution
{
public:
    int wiggleMaxLength(vector<int>& nums)
    {
        if(nums.empty())
            return 0;
        vector<int> maxLength(nums.size(), 1), sign(nums.size(), 0), lastValue(nums.size());
        for(int i = 1; i < nums.size(); i++)
            if(sign[i - 1] > 0 && nums[i - 1] > nums[i])
                maxLength[i] = maxLength[i - 1] + 1, sign[i] = -1, lastValue[i] = nums[i];
            else if(sign[i - 1] < 0 && nums[i - 1] < nums[i])
                maxLength[i] = maxLength[i - 1] + 1, sign[i] = 1, lastValue[i] = nums[i];
            else if(sign[i - 1] == 0 && nums[i - 1] != nums[i])
                maxLength[i] = 2, sign[i] = nums[i - 1] < nums[i]? 1: -1, lastValue[i] = nums[i];
            else
                maxLength[i] = maxLength[i - 1], sign[i] = sign[i - 1], lastValue[i] = lastValue[i - 1];
        return maxLength.back();
    }
};
// @lc code=end
