/*
 * @lc app=leetcode id=219 lang=cpp
 *
 * [219] Contains Duplicate II
 */

// @lc code=start
class Solution
{
public:
    bool containsNearbyDuplicate(vector<int>& nums, int k)
    {
        unordered_map<int, int> num2idx;
        for(int i = 0; i < nums.size(); i++)
        {
            if(num2idx.count(nums[i]) && i - num2idx[nums[i]] <= k)
                return true;
            num2idx[nums[i]] = i;
        }
        return false;
    }
};
// @lc code=end
