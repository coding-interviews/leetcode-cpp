/*
 * @lc app=leetcode id=67 lang=cpp
 *
 * [67] Add Binary
 */

// @lc code=start
class Solution
{
public:
    string addBinary(const string &a, const string &b)
    {
        string result;
        int carry = 0;
        for(int i = a.length() - 1, j = b.length() - 1; i >= 0 || j >= 0; i--, j--)
        {
            if(i >= 0)
                carry += a[i] - '0';
            if(j >= 0)
                carry += b[j] - '0';
            result.push_back('0' + (carry & 1));
            carry >>= 1;
        }
        if(carry)
            result.push_back('1');
        reverse(result.begin(), result.end());
        return result;
    }
};
// @lc code=end
