/*
 * @lc app=leetcode id=415 lang=cpp
 *
 * [415] Add Strings
 */

// @lc code=start
class Solution
{
public:
	string addStrings(const string &num1, const string &num2)
	{
		string num;
		num.reserve(max(num1.length(), num2.length()) + 1);
		int carry = 0;
		for (int i = num1.length() - 1, j = num2.length() - 1; i >= 0 || j >= 0; i--, j--)
		{
			if (i >= 0)
				carry += num1[i] - '0';
			if (j >= 0)
				carry += num2[j] - '0';
			num.push_back('0' + carry % 10);
			carry /= 10;
		}
		if (carry)
			num.push_back('0' + carry);
		reverse(num.begin(), num.end());
		return num;
	}
};
// @lc code=end
