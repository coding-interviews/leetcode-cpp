/*
 * @lc app=leetcode id=375 lang=cpp
 *
 * [375] Guess Number Higher or Lower II
 */

// @lc code=start
class Solution
{
public:
    int getMoneyAmount(int n)
    {
        vector<vector<int>> dp(n + 1, vector<int>(n + 1, 0));
        for(int i = n - 1; i >= 1; i--)
            for(int j = i + 1; j <= n; j++)
            {
                dp[i][j] = INT_MAX;
                for(int k = (i + j) / 2; k <= j; k++)
                    dp[i][j] = min(dp[i][j], k + max(i <= k - 1? dp[i][k - 1]: 0,
                                                     k + 1 <= j? dp[k + 1][j]: 0));
            }
        return dp[1][n];
    }
};
// @lc code=end
