class Solution
{
private:
    void subsets(vector<vector<int>> &result, vector<int> &path, vector<int> &nums, int i)
    {
        result.push_back(path);
        for (int next = i; next < nums.size();)
        {
            path.push_back(nums[next]);
            subsets(result, path, nums, next + 1);
            path.pop_back();
            do
                next++;
            while (next < nums.size() && nums[next - 1] == nums[next]);
        }
    }

public:
    vector<vector<int>> subsetsWithDup(vector<int> &nums)
    {
        sort(nums.begin(), nums.end());
        vector<vector<int>> result;
        vector<int> path;
        subsets(result, path, nums, 0);
        return result;
    }
};
