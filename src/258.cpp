/*
 * @lc app=leetcode id=258 lang=cpp
 *
 * [258] Add Digits
 */

// @lc code=start
class Solution
{
public:
    int addDigits(int num)
    {
        // https://en.wikipedia.org/wiki/Digital_root
        return num? 1 + ((num - 1) % 9): 0;
    }
};
// @lc code=end
