/*
 * @lc app=leetcode id=264 lang=cpp
 *
 * [264] Ugly Number II
 */

// @lc code=start
class Solution
{
public:
    int nthUglyNumber(int n)
    {
        vector<int> numbers(n, 1);
        const int *p2 = numbers.data(), *p3 = p2, *p5 = p2;
        for (int i = 1; i < n; i++)
        {
            numbers[i] = min(min(*p2 * 2, *p3 * 3), *p5 * 5);
            if (numbers[i] == *p2 * 2)
                p2++;
            if (numbers[i] == *p3 * 3)
                p3++;
            if (numbers[i] == *p5 * 5)
                p5++;
        }
        return numbers.back();
    }
};

// @lc code=end
