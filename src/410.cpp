/*
 * @lc app=leetcode id=410 lang=cpp
 *
 * [410] Split Array Largest Sum
 */

// @lc code=start
class Solution
{
public:
    int splitArray(vector<int> &nums, int m)
    {
        vector<vector<long long>> dp(nums.size() + 1, vector<long long>(m + 1, INT_MAX));
        vector<long long> sums(nums.size() + 1, 0);
        for (int i = 0; i < nums.size(); i++)
            sums[i + 1] = sums[i] + nums[i];
        dp[0][0] = 0;
        for (int i = 1; i <= nums.size(); i++)
            for (int w = 1; w <= m; w++)
                for (int k = 0; k < i; k++)
                    dp[i][w] = min(dp[i][w], max(dp[k][w - 1], sums[i] - sums[k]));
        return dp.back().back();
    }
};
// @lc code=end
