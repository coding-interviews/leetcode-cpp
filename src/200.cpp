/*
 * @lc app=leetcode id=200 lang=cpp
 *
 * [200] Number of Islands
 */

// @lc code=start
class Solution
{
private:
    static const int dx[];
    static const int dy[];

    void bfs(vector<vector<char>> &grid, int i, int j)
    {
        int M = grid.size(), N = grid.front().size();
        queue<pair<int, int>> q({{i, j}});
        while(!q.empty())
        {
            pair<int, int> cur = q.front();
            q.pop();
            if(grid[cur.first][cur.second] == '0')
                continue;
            grid[cur.first][cur.second] = '0';
            for(int dir = 0; dir < 4; dir++)
            {
                int x = cur.first + dx[dir], y = cur.second + dy[dir];
                if(x >= 0 && x < M && y >= 0 && y < N && grid[x][y] == '1')
                    q.push({x, y});
            }
        }
    }

public:
    int numIslands(vector<vector<char>> &grid)
    {
        if(grid.empty() || grid.front().empty())
            return 0;
        int M = grid.size(), N = grid.front().size(), result = 0;
        for(int i = 0; i < M; i++)
            for(int j = 0; j < N; j++)
                if(grid[i][j] == '1')
                {
                    bfs(grid, i, j);
                    result++;
                }
        return result;
    }
};


const int Solution::dx[] = {-1, 0, 1, 0};
const int Solution::dy[] = {0, -1, 0, 1};

// @lc code=end
