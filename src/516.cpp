/*
 * @lc app=leetcode id=516 lang=cpp
 *
 * [516] Longest Palindromic Subsequence
 */

// @lc code=start
class Solution
{
public:
    int longestPalindromeSubseq(string &s)
    {
        if (s.empty())
            return 0;
        vector<vector<int>> dp(s.length(), vector<int>(s.length(), 0));
        // dp[i][j]表示s[i~j]中最长的回文子序列的长度
        for (int i = 0; i < s.length(); i++)
            dp[i][i] = 1;
        for (int i = s.length() - 1; i >= 0; i--)
            for (int j = i + 1; j < s.length(); j++)
                if (s[i] == s[j])
                    dp[i][j] = dp[i + 1][j - 1] + 2;
                else
                    dp[i][j] = max(dp[i][j - 1], dp[i + 1][j]);
        return dp.front().back();
    }
};

// @lc code=end
