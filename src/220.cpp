/*
 * @lc app=leetcode id=220 lang=cpp
 *
 * [220] Contains Duplicate III
 */

// @lc code=start
class Solution
{
public:
    bool containsNearbyAlmostDuplicate(vector<int>& nums, int k, int t)
    {
        map<long long, int> num2idx;
        for(int i = 0; i < nums.size(); i++)
        {
            auto it = num2idx.lower_bound((long long)nums[i] - t);
            if(it != num2idx.end() && it->first <= (long long)nums[i] + t)
                return true;
            num2idx[nums[i]] = i;
            if(i >= k)
                num2idx.erase(nums[i - k]);
        }
        return false;
    }
};
// @lc code=end
