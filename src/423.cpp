/*
 * @lc app=leetcode id=423 lang=cpp
 *
 * [423] Reconstruct Original Digits from English
 */

// @lc code=start
class Solution
{
private:
	void handle(string& result, int dict[10][26], int sdict[26], int num, char key)
	{
		int count = sdict[key - 'a'];
		for (int n = 0; n < count; n++)
			result.push_back('0' + num);
		for (int i = 0; i < 26; i++)
			sdict[i] -= dict[num][i] * count;
	}

public:
	string originalDigits(string s)
	{
		const string words[10] = { "zero",
			"one","two","three","four","five",
			"six","seven","eight","nine" };
		int dict[10][26] = { {0} }, sdict[26] = { 0 };
		for (int i = 0; i < 10; i++)
			for (char ch : words[i])
				dict[i][ch - 'a']++;
		for (char ch : s)
			sdict[ch - 'a']++;
		
		string result;
		handle(result, dict, sdict, 0, 'z');
		handle(result, dict, sdict, 2, 'w');
		handle(result, dict, sdict, 6, 'x');
		handle(result, dict, sdict, 7, 's');
		handle(result, dict, sdict, 8, 'g');
		handle(result, dict, sdict, 3, 'h');
		handle(result, dict, sdict, 5, 'v');
		handle(result, dict, sdict, 4, 'r');
		handle(result, dict, sdict, 9, 'i');
		handle(result, dict, sdict, 1, 'o');
		sort(result.begin(), result.end());
		return result;
	}
};
// @lc code=end
