/*
 * @lc app=leetcode id=357 lang=cpp
 *
 * [357] Count Numbers with Unique Digits
 */

// @lc code=start
class Solution
{
public:
    int countNumbersWithUniqueDigits(int n)
    {
        // f(0) = 1
        // f(1) = 10
        // f(2) = 9 × 9
        // f(3) = 9 × 9 × 8
        // f(4) = 9 × 9 × 8 × 7
        // f(n) = 9 × 9 × 8 × ... × (11 - n)
        // result = f(1) + f(2) + ... + f(n) (n > 0)
        //          f(0)                     (n = 0)
        if (n == 0)
            return 1;
        int f[9] = { 1, 10 };
        for(int i = 2; i <= n; i++)
        {
            f[i] = 9;
            for(int j = 1; j < i; j++)
                f[i] *= 10 - j;
        }
        return accumulate(f + 1, f + sizeof(f) / sizeof(int), 0);
    }
};
// @lc code=end
