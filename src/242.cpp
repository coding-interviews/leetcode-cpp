/*
 * @lc app=leetcode id=242 lang=cpp
 *
 * [242] Valid Anagram
 */

// @lc code=start
class Solution
{
public:
    bool isAnagram(string s, string t)
    {
        int counter1[26] = { 0 }, counter2[26] = { 0 };
        for(char ch: s)
            counter1[ch - 'a']++;
        for(char ch: t)
            counter2[ch - 'a']++;
        return memcmp(counter1, counter2, sizeof(counter1)) == 0;
    }
};
// @lc code=end
