class Solution
{
public:
    int minPathSum(vector<vector<int>> &grid)
    {
        if (grid.empty() || grid.front().empty())
            return 0;
        int M = grid.size(), N = grid.front().size();
        vector<int> dp(N, INT_MAX);
        dp[0] = grid[0][0];
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                if (i > 0 || j > 0)
                    dp[j] = min(dp[j], j > 0 ? dp[j - 1] : INT_MAX) + grid[i][j];
        return dp.back();
    }
};
