/*
 * @lc app=leetcode id=201 lang=cpp
 *
 * [201] Bitwise AND of Numbers Range
 */

// @lc code=start
class Solution
{
public:
    int rangeBitwiseAnd(int m, int n)
    {
        int result = 0;
        for (int i = 31; i >= 0; i--)
        {
            int x = m >> i & 1, y = n >> i & 1;
            result |= (x & y) << i;
            if (x != y)
                break;
        }
        return result;
    }
};

// @lc code=end
