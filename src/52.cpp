class Solution
{
private:
    bool isValid(vector<string> &board, int i, int j)
    {
        int n = board.size();
        // 检查当前列是否有重复
        for (int row = 0; row < n; row++)
            if (board[row][j] == 'Q')
                return false;
        // 检查左上->右下对角线是否有重复
        for (int row = i - 1, col = j - 1; row >= 0 && col >= 0; row--, col--)
            if (board[row][col] == 'Q')
                return false;
        for (int row = i + 1, col = j + 1; row < n && col < n; row++, col++)
            if (board[row][col] == 'Q')
                return false;
        // 检查右上->左下对角线是否有重复
        for (int row = i - 1, col = j + 1; row >= 0 && col < n; row--, col++)
            if (board[row][col] == 'Q')
                return false;
        for (int row = i + 1, col = j - 1; row < n && col >= 0; row++, col--)
            if (board[row][col] == 'Q')
                return false;
        return true;
    }

    int solveNQueens(int n, int i, vector<string> &cur)
    {
        if (i >= n)
            return 1;
        int solutions = 0;
        for (int j = 0; j < n; j++)
        {
            if (!isValid(cur, i, j))
                continue;
            cur[i][j] = 'Q';
            solutions += solveNQueens(n, i + 1, cur);
            cur[i][j] = '.';
        }
        return solutions;
    }

public:
    int totalNQueens(int n)
    {
        vector<string> cur(n, string(n, '.'));
        return solveNQueens(n, 0, cur);
    }
};
