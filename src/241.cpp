/*
 * @lc app=leetcode id=241 lang=cpp
 *
 * [241] Different Ways to Add Parentheses
 */

// @lc code=start
class Solution
{
public:
    vector<int> diffWaysToCompute(string &&input)
    {
        vector<int> result;
        for (int i = 0; i < input.length(); i++)
            if (input[i] == '+' || input[i] == '-' || input[i] == '*')
            {
                vector<int> left = diffWaysToCompute(input.substr(0, i));
                vector<int> right = diffWaysToCompute(input.substr(i + 1));
                for (int x = 0; x < left.size(); x++)
                    for (int y = 0; y < right.size(); y++)
                        switch (input[i])
                        {
                        case '+':
                            result.push_back(left[x] + right[y]);
                            break;
                        case '-':
                            result.push_back(left[x] - right[y]);
                            break;
                        case '*':
                            result.push_back(left[x] * right[y]);
                            break;
                        }
            }
        return !result.empty() ? result : vector<int>({atoi(input.c_str())});
    }

    vector<int> diffWaysToCompute(string &input)
    {
        return diffWaysToCompute(move(input));
    }
};
// @lc code=end
