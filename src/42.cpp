class Solution
{
public:
    int trap(vector<int> &height)
    {
        if (height.empty())
            return 0;
        int left = 0, right = height.size() - 1, water = 0;
        while (left < right)
        {
            if (height[left] < height[right])
            {
                water += max(0, height[left] - height[left + 1]);
                height[left + 1] = max(height[left], height[left + 1]);
                left++;
            }
            else
            {
                water += max(0, height[right] - height[right - 1]);
                height[right - 1] = max(height[right], height[right - 1]);
                right--;
            }
        }
        return water;
    }
};
