/*
 * @lc app=leetcode id=132 lang=cpp
 *
 * [132] Palindrome Partitioning II
 */

// @lc code=start
class Solution
{
private:
    bool palindrome(const string &s, int left, int right)
    {
        while (left < right)
            if (s[left++] != s[right--])
                return false;
        return true;
    }

public:
    int minCut(string &s)
    {
        if (s.empty())
            return 0;
        vector<int> dp(s.length() + 1, INT_MAX);
        dp[0] = -1, dp[1] = 0;
        for (int n = 2; n <= s.length(); n++)
            for (int left = 0; left <= n - 1; left++)
                if (palindrome(s, left, n - 1))
                    dp[n] = min(dp[n], dp[left] + 1);
        return dp.back();
    }
};
// @lc code=end
