/*
 * @lc app=leetcode id=368 lang=cpp
 *
 * [368] Largest Divisible Subset
 */

// @lc code=start
class Solution
{
public:
    vector<int> largestDivisibleSubset(vector<int>& nums)
    {
        if(nums.empty())
            return {};
        sort(nums.begin(), nums.end());
        vector<int> dp(nums.size(), 1), parent(nums.size(), -1);
        for(int i = 1; i < nums.size(); i++)
            for(int j = 0; j < i; j++)
                if(nums[i] % nums[j] == 0 && dp[j] + 1 > dp[i])
                    dp[i] = dp[j] + 1, parent[i] = j;
        vector<int> result;
        for(int i = max_element(dp.begin(), dp.end()) - dp.begin(); i >= 0; i = parent[i])
            result.push_back(nums[i]);
        return result;
    }
};
// @lc code=end
