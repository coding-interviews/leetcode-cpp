class Solution
{
public:
    int numTrees(int n)
    {
        vector<vector<int>> dp(n + 1, vector<int>(n + 1, 1));
        for (int left = n; left >= 1; left--)
            for (int right = left + 1; right <= n; right++)
            {
                dp[left][right] = 0;
                for (int i = left; i <= right; i++)
                    dp[left][right] += (left < i - 1 ? dp[left][i - 1] : 1) * (i + 1 < right ? dp[i + 1][right] : 1);
            }
        return dp[1][n];
    }
};
