/*
 * @lc app=leetcode id=223 lang=cpp
 *
 * [223] Rectangle Area
 */

// @lc code=start
class Solution
{
public:
	int computeArea(int A, int B, int C, int D, int E, int F, int G, int H)
	{
		int top = min(D, H), bottom = max(B, F), left = max(A, E), right = min(C, G);
		if (right > left && top > bottom)
			return (long long)(C - A) * (D - B) + (G - E) * (H - F) - (right - left) * (top - bottom);
		else
			return (C - A) * (D - B) + (G - E) * (H - F);
	}
};
// @lc code=end
