/*
 * @lc app=leetcode id=190 lang=cpp
 *
 * [190] Reverse Bits
 */

// @lc code=start

class Solution
{
public:
    uint32_t reverseBits(uint32_t n)
    {
        uint32_t result = 0;
        for(int i = 0; i < 4; i++)
        {
            result = result << 8 | ((n & 0xff) * 0x0202020202 & 0x010884422010) % 1023;
            n >>= 8;
        }
        return result;
    }
};
// @lc code=end
