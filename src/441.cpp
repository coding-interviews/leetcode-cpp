/*
 * @lc app=leetcode id=441 lang=cpp
 *
 * [441] Arranging Coins
 */

// @lc code=start
class Solution
{
public:
    int arrangeCoins(int n)
    {
        return (-1 + sqrt(1 + 8.0 * n)) / 2;
    }
};
// @lc code=end
