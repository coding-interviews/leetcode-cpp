/*
 * @lc app=leetcode id=227 lang=cpp
 *
 * [227] Basic Calculator II
 */

// @lc code=start
inline bool is_digit(char ch)
{
    return '0' <= ch && ch <= '9';
}

class Solution
{
public:
    int calculate(string &s)
    {
        int num = 0;
        char op = '+';
        vector<int> st;
        for (int i = 0; i < s.size(); i++)
        {
            if (is_digit(s[i]))
                num = num * 10 + (s[i] - '0');
            if (!is_digit(s[i]) && s[i] != ' ' || i == s.size() - 1)
            {
                if (op == '*')
                    st.back() *= num;
                else if (op == '/')
                    st.back() /= num;
                else if (op == '+')
                    st.push_back(num);
                else
                    st.push_back(-num);
                op = s[i], num = 0;
            }
        }
        return accumulate(st.begin(), st.end(), 0);
    }
};
// @lc code=end
