class Solution
{
public:
    int threeSumClosest(vector<int> &nums, int target)
    {
        sort(nums.begin(), nums.end());
        int result = 0, mindiff = INT_MAX;
        for (int left = 0; left < nums.size() - 2;)
        {
            for (int mid = left + 1, right = nums.size() - 1; mid < right;)
            {
                int sum = nums[left] + nums[mid] + nums[right] - target;
                if (abs(sum) < mindiff)
                    mindiff = abs(sum), result = nums[left] + nums[mid] + nums[right];
                if (sum <= 0)
                    do
                        mid++;
                    while (mid < right && nums[mid - 1] == nums[mid]);
                else
                    do
                        right--;
                    while (mid < right && nums[right] == nums[right + 1]);
            }
            do
                left++;
            while (left < nums.size() - 2 && nums[left - 1] == nums[left]);
        }
        return result;
    }
};