/*
 * @lc app=leetcode id=289 lang=cpp
 *
 * [289] Game of Life
 */

// @lc code=start
class Solution
{
private:
    int liveNeighbors(vector<vector<int>> &board, int i, int j)
    {
        const int dx[] = {-1, 0, 1, 0, -1, 1, -1, 1};
        const int dy[] = {0, -1, 0, 1, -1, -1, 1, 1};
        int M = board.size(), N = board.front().size();
        int count = 0;
        for (int dir = 0; dir < sizeof(dx) / sizeof(int); dir++)
        {
            int x = i + dx[dir], y = j + dy[dir];
            if (x >= 0 && x < M && y >= 0 && y < N && (board[x][y] == 1 || board[x][y] == 2))
                count++;
        }
        return count;
    }

public:
    void gameOfLife(vector<vector<int>> &board)
    {
        if (board.empty() || board.front().empty())
            return;
        int M = board.size(), N = board.front().size();
        // 0: dead, 1: live 2: live->dead 3: dead->live
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
            {
                if (board[i][j] == 1)
                {
                    int lives = liveNeighbors(board, i, j);
                    if (lives < 2 || lives > 3)
                        board[i][j] = 2;
                }
                else if (board[i][j] == 0)
                {
                    if (liveNeighbors(board, i, j) == 3)
                        board[i][j] = 3;
                }
            }
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                if (board[i][j] >= 2)
                    board[i][j] -= 2;
    }
};

// @lc code=end
