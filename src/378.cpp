/*
 * @lc app=leetcode id=378 lang=cpp
 *
 * [378] Kth Smallest Element in a Sorted Matrix
 */

// @lc code=start
class Solution
{
public:
    int kthSmallest(vector<vector<int>> &matrix, int k)
    {
        struct Item
        {
            int x, y, val;

            Item(int x, int y, int val) : x(x), y(y), val(val) {}

            bool operator<(const Item &right) const
            {
                return this->val > right.val;
            }
        };
        priority_queue<Item> heap;
        heap.push({0, 0, matrix[0][0]});
        vector<vector<bool>> visited(matrix.size(), vector<bool>(matrix.size(), false));
        for (int i = 0; i < k - 1; i++)
        {
            Item p = heap.top();
            heap.pop();
            if (p.x + 1 < matrix.size() && !visited[p.x + 1][p.y])
            {
                heap.push({p.x + 1, p.y, matrix[p.x + 1][p.y]});
                visited[p.x + 1][p.y] = true;
            }
            if (p.y + 1 < matrix.size() && !visited[p.x][p.y + 1])
            {
                heap.push({p.x, p.y + 1, matrix[p.x][p.y + 1]});
                visited[p.x][p.y + 1] = true;
            }
        }
        return heap.top().val;
    }
};
// @lc code=end
