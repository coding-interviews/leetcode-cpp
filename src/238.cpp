/*
 * @lc app=leetcode id=238 lang=cpp
 *
 * [238] Product of Array Except Self
 */

// @lc code=start
class Solution
{
public:
    vector<int> productExceptSelf(vector<int> &nums)
    {
        vector<int> result(nums.size(), 1);
        for (int i = 1; i < nums.size(); i++)
            result[i] = result[i - 1] * nums[i - 1];

        for (int i = nums.size() - 2, tmp = 1; i >= 0; i--)
        {
            int val = tmp;
            tmp = nums[i];
            nums[i] = nums[i + 1] * val;
        }
        nums.back() = 1;

        for (int i = 0; i < nums.size(); i++)
            result[i] *= nums[i];
        return result;
    }
};
// @lc code=end
