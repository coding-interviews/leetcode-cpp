/*
 * @lc app=leetcode id=334 lang=cpp
 *
 * [334] Increasing Triplet Subsequence
 */

// @lc code=start
class Solution
{
public:
    bool increasingTriplet(vector<int> &nums)
    {
        int minval = INT_MAX, midval = INT_MAX;
        for (int num : nums)
            if (num <= minval)
                minval = num;
            else if (num <= midval)
                midval = num;
            else
                return true;
        return false;
    }
};
// @lc code=end
