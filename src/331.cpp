class Solution
{
public:
    bool isValidSerialization(string preorder)
    {
        stringstream ss(preorder);
        int slots = 1;
        for (string token; getline(ss, token, ',');)
        {
            slots--;
            if (slots < 0)
                return false;
            if (token != "#")
                slots += 2;
        }
        return slots == 0;
    }
};
