class Solution
{
public:
    vector<vector<int>> generateMatrix(int n)
    {
        vector<vector<int>> matrix(n, vector<int>(n));
        for (int num = 0, level = 0; level <= n / 2; level++)
        {
            for (int j = level; j < n - level; j++)
                matrix[level][j] = ++num;
            if (level + 1 >= n - level)
                return matrix; // 下面的循环不满足初始条件时，后面的也不用做了
            for (int i = level + 1; i < n - level; i++)
                matrix[i][n - level - 1] = ++num;
            if (n - level - 2 < level)
                return matrix; // 下面的循环不满足初始条件时，后面的也不用做了
            for (int j = n - level - 2; j >= level; j--)
                matrix[n - level - 1][j] = ++num;
            for (int i = n - level - 2; i > level; i--)
                matrix[i][level] = ++num;
        }
        return matrix;
    }
};
