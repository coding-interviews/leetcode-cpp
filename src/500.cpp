/*
 * @lc app=leetcode id=500 lang=cpp
 *
 * [500] Keyboard Row
 */

// @lc code=start
constexpr char upper(char ch)
{
    return 'a' <= ch && ch <= 'z'? ch - 'a' + 'A': ch;
}

class Solution
{
private:
    void initKeyboard(int keyboard[26])
    {
        const string s[3] =
        {
            "QWERTYUIOP",
            "ASDFGHJKL",
            "ZXCVBNM"
        };
        for(int i = 0; i < 3; i++)
            for(char ch: s[i])
                keyboard[ch - 'A'] = i;
    }

public:
    vector<string> findWords(vector<string>& words)
    {
        int keyboard[26];
        memset(keyboard, -1, sizeof(keyboard));
        initKeyboard(keyboard);
        vector<string> result;
        for(const string &word: words)
        {
            if(word.empty())
                continue;
            int category = keyboard[upper(word[0]) - 'A'];
            result.push_back(word);
            for(int i = 1; i < word.length(); i++)
                if(keyboard[upper(word[i]) - 'A'] != category)
                {
                    result.pop_back();
                    break;
                }
        }
        return result;
    }
};
// @lc code=end
