class Solution
{
public:
    int uniquePathsWithObstacles(vector<vector<int>> &obstacleGrid)
    {
        int m = obstacleGrid.size(), n = obstacleGrid.front().size();
        // 设dp[i][j]表示从(0,0)到(i,j)的路径数量
        // dp[i][j] = dp[i - 1][j] + dp[i][j - 1]
        // 使用滚动数组优化空间复杂度到O(n)
        vector<long long> dp(n, 0);
        dp[0] = !obstacleGrid[0][0];
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                if (obstacleGrid[i][j])
                    dp[j] = 0;
                else if (j > 0)
                    dp[j] += dp[j - 1];
        return dp.back();
    }
};
