class Solution
{
private:
    void setMarks(vector<vector<int>> &matrix, int i, int j)
    {
        for (int col = 0; col < matrix.front().size(); col++)
            if (matrix[i][col])
                matrix[i][col] = INT_MIN / 2;
        for (int row = 0; row < matrix.size(); row++)
            if (matrix[row][j])
                matrix[row][j] = INT_MIN / 2;
    }

public:
    void setZeroes(vector<vector<int>> &matrix)
    {
        if (matrix.empty() || matrix.front().empty())
            return;
        int M = matrix.size(), N = matrix.front().size();
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                if (matrix[i][j] == 0)
                    setMarks(matrix, i, j);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                if (matrix[i][j] == INT_MIN / 2)
                    matrix[i][j] = 0;
    }
};
