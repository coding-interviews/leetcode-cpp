/*
 * @lc app=leetcode id=647 lang=cpp
 *
 * [647] Palindromic Substrings
 */

// @lc code=start
class Solution
{
public:
    int countSubstrings(string &s)
    {
        vector<vector<bool>> dp(s.length(), vector<bool>(s.length(), true));
        int count = 0;
        // dp[i][j] = dp[i+1][j-1] && s[i] == s[j]
        for (int i = s.length() - 1; i >= 0; i--)
            for (int j = i; j < s.length(); j++)
            {
                dp[i][j] = s[i] == s[j];
                if (i + 1 <= j - 1)
                    dp[i][j] = dp[i][j] && dp[i + 1][j - 1];
                if (dp[i][j])
                    count++;
            }
        return count;
    }
};
// @lc code=end
