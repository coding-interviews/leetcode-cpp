/*
 * @lc app=leetcode id=413 lang=cpp
 *
 * [413] Arithmetic Slices
 */

// @lc code=start
/*
 * @lc app=leetcode id=413 lang=cpp
 *
 * [413] Arithmetic Slices
 */
class Solution
{
public:
    int numberOfArithmeticSlices(vector<int> &A)
    {
        if (A.size() < 3)
            return 0;
        int left = 0, diff = A.front(), result = 0;
        for (int right = 0; right < A.size(); right++)
        {
            if (right > 0 && A[right] - A[right - 1] != diff)
                diff = A[right] - A[right - 1], left = right - 1;
            result += max(0, right - left - 1);
        }
        return result;
    }
};

// @lc code=end
