class Solution
{
public:
    int uniquePaths(int m, int n)
    {
        // 设dp[i][j]表示从(0,0)到(i,j)的路径数量
        // dp[i][j] = dp[i - 1][j] + dp[i][j - 1]
        // 使用滚动数组优化空间复杂度到O(n)
        vector<int> dp(n, 0);
        dp[0] = 1;
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                if (j > 0)
                    dp[j] += dp[j - 1];
        return dp.back();
    }
};
