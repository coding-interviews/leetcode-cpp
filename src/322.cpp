/*
 * @lc app=leetcode id=322 lang=cpp
 *
 * [322] Coin Change
 */

// @lc code=start
class Solution
{
public:
    int coinChange(vector<int> &coins, int amount)
    {
        vector<unsigned int> dp(amount + 1, INT_MAX);
        dp[0] = 0;
        for (int i = 1; i <= coins.size(); i++)
            for (int w = coins[i - 1]; w <= amount; w++)
                dp[w] = min(dp[w], dp[w - coins[i - 1]] + 1);
        return dp.back() < INT_MAX ? dp.back() : -1;
    }
};

// @lc code=end
