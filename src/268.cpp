/*
 * @lc app=leetcode id=268 lang=cpp
 *
 * [268] Missing Number
 */

// @lc code=start
class Solution
{
public:
    int missingNumber(vector<int> &nums)
    {
        for (int i = 0; i < nums.size(); i++)
            while (nums[i] < nums.size() && nums[i] != i)
                swap(nums[i], nums[nums[i]]);
        for (int i = 0; i < nums.size(); i++)
            if (nums[i] != i)
                return i;
        return nums.size();
    }
};
// @lc code=end
