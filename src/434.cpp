/*
 * @lc app=leetcode id=434 lang=cpp
 *
 * [434] Number of Segments in a String
 */

// @lc code=start
class Solution
{
public:
    int countSegments(string s)
    {
        bool isSpace = true;
        int count = 0;
        for(char ch: s)
        {
            if(isSpace && ch != ' ')
                count++;
            isSpace = ch == ' ';
        }
        return count;
    }
};
// @lc code=end
