/*
 * @lc app=leetcode id=318 lang=cpp
 *
 * [318] Maximum Product of Word Lengths
 */

// @lc code=start
class Solution
{
public:
    int maxProduct(vector<string>& words)
    {
        vector<int> wordSets(words.size());
        size_t maxproduct = 0;
        for(int i = 0; i < words.size(); i++)
        {
            for(char ch: words[i])
                wordSets[i] |= 1 << (ch - 'a');
            for(int j = 0; j < i; j++)
                if(!(wordSets[i] & wordSets[j]))
                    maxproduct = max(maxproduct, words[i].length() * words[j].length());
        }
        return maxproduct;
    }
};
// @lc code=end
