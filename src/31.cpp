class Solution
{
public:
    void nextPermutation(vector<int> &nums)
    {
        if (nums.size() <= 1)
            return;
        // 从右向左找出第一个左<右的位置
        int left = nums.size() - 2;
        while (nums[left] >= nums[left + 1])
        {
            left--;
            if (left < 0)
            {
                // 已经是最后一个排列
                reverse(nums.begin(), nums.end());
                return;
            }
        }
        // 从右向左找出第一个比nums[left]大的数
        int right = nums.size() - 1;
        while (left < right && nums[left] >= nums[right])
            right--;
        // 交换nums[left]和nums[right]
        swap(nums[left], nums[right]);
        // 逆序nums[left]右边的数
        reverse(nums.begin() + left + 1, nums.end());
    }
};
