/*
 * @lc app=leetcode id=130 lang=cpp
 *
 * [130] Surrounded Regions
 */

// @lc code=start
typedef pair<int, int> Point;

class Solution
{
private:
    template <char src, char dst>
    void bfs(vector<vector<char>> &board, int i, int j)
    {
        const int dx[] = {-1, 0, 1, 0}, dy[] = {0, -1, 0, 1};
        int M = board.size(), N = board.front().size();
        queue<Point> q({{i, j}});
        while(!q.empty())
        {
            Point p = q.front();
            q.pop();
            if(board[p.first][p.second] != src)
                continue;
            board[p.first][p.second] = dst;
            for(int dir = 0; dir < 4; dir++)
            {
                int x = p.first + dx[dir], y = p.second + dy[dir];
                if(x >= 0 && x < M && y >= 0 && y < N && board[x][y] == src)
                    q.push({x, y});
            }
        }
    }

public:
    void solve(vector<vector<char>>& board)
    {
        if(board.empty() || board.front().empty())
            return;
        int M = board.size(), N = board.front().size();
        // 把边缘部分的O全部涂成T
        for(int i = 0; i < M; i++)
        {
            bfs<'O', 'T'>(board, i, 0);
            bfs<'O', 'T'>(board, i, N - 1);
        }
        for(int j = 0; j < N; j++)
        {
            bfs<'O', 'T'>(board, 0, j);
            bfs<'O', 'T'>(board, M - 1, j);
        }
        // 把所有O全涂成X
        for(int i = 0; i < M; i++)
            for(int j = 0; j < N; j++)
                if(board[i][j] == 'O')
                    board[i][j] = 'X';
        // 把所有T全涂成O
        for(int i = 0; i < M; i++)
            for(int j = 0; j < N; j++)
                if(board[i][j] == 'T')
                    board[i][j] = 'O';
    }
};
// @lc code=end
