/*
 * @lc app=leetcode id=486 lang=cpp
 *
 * [486] Predict the Winner
 */

// @lc code=start
class Solution
{
public:
    bool PredictTheWinner(vector<int> &nums)
    {
        vector<int> dp(nums.size());
        for (int i = nums.size() - 1; i >= 0; i--)
            for (int j = i; j < nums.size(); j++)
                dp[j] = max(nums[i] - (i + 1 <= j ? dp[j] : 0),
                            nums[j] - (j - 1 >= i ? dp[j - 1] : 0));
        return dp[nums.size() - 1] >= 0;
    }
};

// @lc code=end
