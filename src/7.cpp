class Solution
{
private:
    template <typename T = long long>
    T reverse(T x)
    {
        T y = 0;
        while (x)
        {
            y = y * 10 + x % 10;
            x /= 10;
        }
        return y;
    }

public:
    int reverse(int x)
    {
        long long y = x >= 0 ? reverse((long long)x) : -reverse(-(long long)x);
        return INT_MIN <= y && y <= INT_MAX ? y : 0;
    }
};
