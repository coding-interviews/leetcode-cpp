/*
 * @lc app=leetcode id=481 lang=cpp
 *
 * [481] Magical String
 */

// @lc code=start
class Solution
{
public:
    int magicalString(int n)
    {
        if (n == 0)
            return 0;
        int result = 1;
        vector<int> nums = {1};
        nums.reserve(n);
        for (int left = 0, right = 1; right < n; right++)
        {
            if (nums[left] == 1)
            {
                nums.push_back(3 - nums.back());
                nums[left++]--;
            }
            else
            {
                nums.push_back(nums.back());
                nums[left]--;
            }
            result += nums.back() == 1;
        }
        return result;
    }
};

// @lc code=end
