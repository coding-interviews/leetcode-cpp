const string MAP[] =
    {
        "",
        "", "abc", "def",
        "ghi", "jkl", "mno",
        "pqrs", "tuv", "wxyz"};

class Solution
{
private:
    void dfs(vector<string> &result, string &path, const char *p)
    {
        if (*p == '\0')
        {
            result.push_back(path);
            return;
        }
        for (char ch : MAP[*p - '0'])
        {
            path.push_back(ch);
            dfs(result, path, p + 1);
            path.pop_back();
        }
    }

public:
    vector<string> letterCombinations(string &digits)
    {
        if (digits.empty())
            return {};
        vector<string> result;
        string path;
        dfs(result, path, digits.c_str());
        return result;
    }
};
