/*
 * @lc app=leetcode id=394 lang=cpp
 *
 * [394] Decode String
 */

// @lc code=start
class Solution
{
public:
    string decodeString(string &s)
    {
        stack<int> st;
        int num = 0;
        for (char ch : s)
        {
            if ('0' <= ch && ch <= '9')
                num = num * 10 + ch - '0';
            else if (ch == '[')
            {
                st.push(-num);
                num = 0;
            }
            else if (ch == ']')
            {
                string data;
                while (st.top() > 0)
                {
                    data.push_back(st.top());
                    st.pop();
                }
                int repeat = -st.top();
                st.pop();
                for (int n = 0; n < repeat; n++)
                    for (int i = data.size() - 1; i >= 0; i--)
                        st.push(data[i]);
            }
            else
                st.push(ch);
        }

        string result;
        result.reserve(st.size());
        while (!st.empty())
        {
            result.push_back(st.top());
            st.pop();
        }
        reverse(result.begin(), result.end());
        return result;
    }
};

// @lc code=end
