/*
 * @lc app=leetcode id=274 lang=cpp
 *
 * [274] H-Index
 */

// @lc code=start
class Solution
{
private:
    // 引用数>=h的论文数量
    int hPapers(vector<int> &citations, int h)
    {
        int count = 0;
        for (int citation : citations)
            if (citation >= h)
                count++;
        return count;
    }

public:
    int hIndex(vector<int> &citations)
    {
        if (citations.empty())
            return 0;
        int left = 0, right = citations.size();
        while (left + 1 < right)
        {
            int mid = left + (right - left) / 2;
            if (hPapers(citations, mid) < mid)
                right = mid;
            else
                left = mid;
        }
        return hPapers(citations, right) >= right ? right : left;
    }
};

// @lc code=end
