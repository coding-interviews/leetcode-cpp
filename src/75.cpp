class Solution
{
private:
    void rainbowSort(vector<int> &nums, int start, int end, int colorFrom, int colorTo)
    {
        if (start >= end || colorFrom >= colorTo)
            return;
        int left = start, right = end;
        int colorMid = colorFrom + (colorTo - colorFrom) / 2;
        while (left <= right)
        {
            while (left <= right && nums[left] <= colorMid)
                left++;
            while (left <= right && nums[right] > colorMid)
                right--;
            if (left <= right)
                swap(nums[left++], nums[right--]);
        }
        rainbowSort(nums, start, right, colorFrom, colorMid);
        rainbowSort(nums, left, end, colorMid + 1, colorTo);
    }

public:
    void sortColors(vector<int> &nums)
    {
        rainbowSort(nums, 0, nums.size() - 1, 0, 2);
    }
};
