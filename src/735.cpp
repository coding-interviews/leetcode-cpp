/*
 * @lc app=leetcode id=735 lang=cpp
 *
 * [735] Asteroid Collision
 */

// @lc code=start
class Solution
{
public:
    vector<int> asteroidCollision(vector<int> &asteroids)
    {
        vector<int> result;
        for (int asteroid : asteroids)
        {
            if (asteroid > 0)
                result.push_back(asteroid);
            else
            {
                while (!result.empty() && result.back() > 0 && result.back() < -asteroid)
                    result.pop_back();
                if (result.empty() || result.back() < 0)
                    result.push_back(asteroid);
                else if (result.back() == -asteroid)
                    result.pop_back();
            }
        }
        return result;
    }
};
// @lc code=end
