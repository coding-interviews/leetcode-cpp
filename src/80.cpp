class Solution
{
public:
    int removeDuplicates(vector<int> &nums)
    {
        for (int i = 0; i < (int)nums.size() - 2; i++)
            for (int j = i + 2; j < nums.size() && nums[i] == nums[j];)
                nums.erase(nums.begin() + j);
        return nums.size();
    }
};
