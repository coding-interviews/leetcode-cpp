/*
 * @lc app=leetcode id=417 lang=cpp
 *
 * [417] Pacific Atlantic Water Flow
 */

// @lc code=start
typedef pair<int, int> Pair;
const int dx[] = {-1, 0, 1, 0}, dy[] = {0, -1, 0, 1};

class Solution
{
private:
    // BFS
    void bfs(vector<vector<int>> &matrix, vector<vector<bool>> &visited, int i, int j)
    {
        int M = matrix.size(), N = matrix.front().size();
        queue<Pair> q({{i, j}});
        while (!q.empty())
        {
            Pair cur = q.front();
            q.pop();
            if (visited[cur.first][cur.second])
                continue;
            visited[cur.first][cur.second] = true;
            for (int d = 0; d < 4; d++)
            {
                int inext = cur.first + dx[d], jnext = cur.second + dy[d];
                if (inext >= 0 && inext < M && jnext >= 0 && jnext < N && matrix[inext][jnext] >= matrix[cur.first][cur.second])
                    q.push({inext, jnext});
            }
        }
    }

public:
    vector<vector<int>> pacificAtlantic(vector<vector<int>> &matrix)
    {
        if (matrix.empty() || matrix.front().empty())
            return {};
        int M = matrix.size(), N = matrix.front().size();
        vector<vector<bool>> pacific(M, vector<bool>(N, false)), atlantic(M, vector<bool>(N, false));
        // 遍历最左侧和最右侧，探索可以到达的点
        for (int j = 0; j < N; j++)
            bfs(matrix, pacific, 0, j);
        for (int i = 0; i < M; i++)
            bfs(matrix, pacific, i, 0);
        // 遍历最右侧和最下侧，探索可以到达的点
        for (int j = 0; j < N; j++)
            bfs(matrix, atlantic, M - 1, j);
        for (int i = 0; i < M; i++)
            bfs(matrix, atlantic, i, N - 1);

        vector<vector<int>> result;
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                if (pacific[i][j] && atlantic[i][j])
                    result.push_back({i, j});
        return result;
    }
};
// @lc code=end
