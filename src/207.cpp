/*
 * @lc app=leetcode id=207 lang=cpp
 *
 * [207] Course Schedule
 */

// @lc code=start
class Solution
{
public:
    bool canFinish(int numCourses, vector<vector<int>> &prerequisites)
    {
        // 有向图转成邻接表，同时统计每个节点的入度
        vector<vector<int>> edges(numCourses);
        vector<int> indegree(numCourses, 0);
        for (const vector<int> &edge : prerequisites)
        {
            edges[edge[1]].push_back(edge[0]);
            indegree[edge[0]]++;
        }

        // 找到入度为0的节点
        queue<int> q;
        for (int i = 0; i < numCourses; i++)
            if (indegree[i] == 0)
                q.push(i);

        // 开始拓扑排序
        vector<int> result;
        while (!q.empty())
        {
            int p = q.front();
            q.pop();
            result.push_back(p);
            for (int next : edges[p])
            {
                indegree[next]--;
                if (indegree[next] == 0)
                    q.push(next);
            }
        }
        return result.size() == numCourses;
    }
};
// @lc code=end
