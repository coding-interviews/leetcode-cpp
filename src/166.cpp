/*
 * @lc app=leetcode id=166 lang=cpp
 *
 * [166] Fraction to Recurring Decimal
 */

// @lc code=start
class Solution
{
public:
	string fractionToDecimal(int numerator, int denominator)
	{
		if (denominator == 0)
			return "NaN";
		else if (numerator == 0)
		{
			stringstream ss;
			ss << numerator;
			return ss.str();
		}
		long long num = numerator, den = denominator;
		int sign = num * den > 0 ? 1 : -1;
		num = abs(num), den = abs(den);

		unordered_map<int, int> dict;
		stringstream ss;
		// 处理整数部分
		if (sign < 0)
			ss << '-';
		ss << num / den;
		// 处理小数部分
		if (num % den)
		{
			num %= den;
			ss << '.';
			vector<int> digits;
			int recurring = -1, i = 0, remainder = 0;
			dict[num] = 0;
			do
			{
				num *= 10;
				remainder = num % den;
				digits.push_back(num / den);
				if (dict.find(remainder) != dict.end())
				{
					recurring = dict[remainder];
					break;
				}
				if (remainder)
					dict[remainder] = ++i;
				num = remainder;
			} while (remainder); // 除尽或出现循环，结束
			if (recurring >= 0)
			{
				for (int i = 0; i < recurring; i++)
					ss << digits[i];
				ss << '(';
				for (int i = recurring; i < digits.size(); i++)
					ss << digits[i];
				ss << ')';
			}
			else
				for (int i = 0; i < digits.size(); i++)
					ss << digits[i];
		}
		return ss.str();
	}
};
// @lc code=end
