/*
 * @lc app=leetcode id=152 lang=cpp
 *
 * [152] Maximum Product Subarray
 */

// @lc code=start
class Solution
{
public:
    int maxProduct(vector<int> &nums)
    {
        // 设maxprod[i]表示以nums[i]结尾的最大连续乘积
        // 设minprod[i]表示以nums[i]结尾的最小连续乘积
        vector<int> maxprod(nums.size()), minprod(nums.size());
        maxprod[0] = minprod[0] = nums[0];
        for (int i = 1; i < nums.size(); i++)
        {
            maxprod[i] = minprod[i] = nums[i];
            if (nums[i] > 0)
            {
                maxprod[i] = max(maxprod[i], maxprod[i - 1] * nums[i]);
                minprod[i] = min(minprod[i], minprod[i - 1] * nums[i]);
            }
            else if (nums[i] < 0)
            {
                maxprod[i] = max(maxprod[i], minprod[i - 1] * nums[i]);
                minprod[i] = min(minprod[i], maxprod[i - 1] * nums[i]);
            }
        }
        return *max_element(maxprod.begin(), maxprod.end());
    }
};
// @lc code=end
