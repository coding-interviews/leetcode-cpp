/*
 * @lc app=leetcode id=229 lang=cpp
 *
 * [229] Majority Element II
 */

// @lc code=start
class Solution
{
public:
    vector<int> majorityElement(vector<int> &nums)
    {
        int num1 = 0, num2 = 1, count1 = 0, count2 = 0;
        for (int num : nums)
        {
            if (num == num1)
                count1++;
            else if (num == num2)
                count2++;
            else if (count1 == 0)
                num1 = num, count1 = 1;
            else if (count2 == 0)
                num2 = num, count2 = 1;
            else
                count1--, count2--;
        }

        vector<int> result;
        if (count(nums.begin(), nums.end(), num1) > nums.size() / 3)
            result.push_back(num1);
        if (count(nums.begin(), nums.end(), num2) > nums.size() / 3)
            result.push_back(num2);
        return result;
    }
};
// @lc code=end
