/*
 * @lc app=leetcode id=407 lang=cpp
 *
 * [407] Trapping Rain Water II
 */

// @lc code=start
class Solution
{
private:
    struct Column
    {
        int x, y, height;

        Column(int x, int y, int height)
            : x(x), y(y), height(height) {}

        bool operator<(const Column &right) const
        {
            return this->height > right.height;
        }
    };

    const int dx[4] = {-1, 0, 0, 1};
    const int dy[4] = {0, -1, 1, 0};

public:
    int trapRainWater(vector<vector<int>> &heightMap)
    {
        if (heightMap.empty() || heightMap.front().empty())
            return 0;
        int M = heightMap.size(), N = heightMap.front().size();
        vector<vector<bool>> visited(M, vector<bool>(N, false));
        // 把周围一圈加入到优先级队列中
        priority_queue<Column> heap;
        for (int i = 0; i < M; i++)
        {
            heap.push({i, 0, heightMap[i].front()});
            if (N > 1)
                heap.push({i, N - 1, heightMap[i].back()});
            visited[i].front() = visited[i].back() = true;
        }
        for (int j = 0; j < N; j++)
        {
            heap.push({0, j, heightMap.front()[j]});
            if (M > 1)
                heap.push({M - 1, j, heightMap.back()[j]});
            visited.front()[j] = visited.back()[j] = true;
        }
        // 按照柱子高度从小到达开始遍历
        int water = 0;
        while (!heap.empty())
        {
            Column current = heap.top();
            heap.pop();
            for (int dir = 0; dir < 4; dir++)
            {
                int x = current.x + dx[dir], y = current.y + dy[dir];
                if (x < 0 || x >= M || y < 0 || y >= N || visited[x][y])
                    continue;
                visited[x][y] = true;
                heap.push({x, y, max(heightMap[x][y], current.height)});
                water += max(0, current.height - heightMap[x][y]);
            }
        }
        return water;
    }
};

// @lc code=end
