/*
 * @lc app=leetcode id=695 lang=cpp
 *
 * [695] Max Area of Island
 */

// @lc code=start
typedef pair<int, int> Point;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, -1, 0, 1};

class Solution
{
private:
    Point find(vector<vector<Point>> &uf, int i, int j)
    {
        if (uf[i][j].first < 0)
            return {i, j};
        else
            return uf[i][j] = find(uf, uf[i][j].first, uf[i][j].second);
    }

public:
    int maxAreaOfIsland(vector<vector<int>> &grid)
    {
        if (grid.empty() || grid.front().empty())
            return 0;
        int M = grid.size(), N = grid.front().size();
        vector<vector<Point>> uf(M, vector<Point>(N, Point(-1, -1)));
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
            {
                if (grid[i][j] == 0)
                    continue;
                Point root = find(uf, i, j);
                for (int dir = 0; dir < 4; dir++)
                {
                    int x = i + dx[dir], y = j + dy[dir];
                    if (x < 0 || x >= M || y < 0 || y >= N || grid[x][y] == 0)
                        continue;
                    Point r = find(uf, x, y);
                    if (r != root)
                        uf[r.first][r.second] = root;
                }
            }

        vector<vector<int>> area(M, vector<int>(N, 0));
        int maxarea = 0;
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                if (grid[i][j])
                {
                    Point root = find(uf, i, j);
                    area[root.first][root.second]++;
                    maxarea = max(maxarea, area[root.first][root.second]);
                }
        return maxarea;
    }
};

// @lc code=end
