/*
 * @lc app=leetcode id=387 lang=cpp
 *
 * [387] First Unique Character in a String
 */

// @lc code=start
class Solution
{
public:
    int firstUniqChar(const string &s)
    {
        int counter[128] = {0};
        for(char ch: s)
            counter[ch]++;
        for(int i = 0; i < s.length(); i++)
            if(counter[s[i]] == 1)
                return i;
        return -1;
    }
};
// @lc code=end
