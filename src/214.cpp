/*
 * @lc app=leetcode id=214 lang=cpp
 *
 * [214] Shortest Palindrome
 */

// @lc code=start
class Solution
{
private:
	bool isPalindrome(const string& s, int right)
	{
		for (int i = 0, j = right; i < j; i++, j--)
			if (s[i] != s[j])
				return false;
		return true;
	}

public:
	string shortestPalindrome(string s)
	{
        if(s.empty())
            return "";
		constexpr size_t BASE = 31, MOD = 1000000007;
		int end = 0;	// 回文字符串结束的位置
		for (size_t i = 0, power = 1, hash_src = 0, hash_rev = 0; i < s.length(); i++)
		{
            if(i > 0)
			    power = (power * BASE) % MOD;
			hash_src = (hash_src * BASE + s[i]) % MOD;
			hash_rev = (hash_rev + s[i] * power) % MOD;
			if (hash_src == hash_rev)
				end = i;
		}
		string prefix = s.substr(end + 1);
		reverse(prefix.begin(), prefix.end());
		return prefix + s;
	}
};
// @lc code=end
