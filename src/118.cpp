/*
 * @lc app=leetcode id=118 lang=cpp
 *
 * [118] Pascal's Triangle
 */

// @lc code=start
class Solution
{
public:
    vector<vector<int>> generate(int numRows)
    {
        if(numRows == 0)
            return {};
        vector<vector<int>> result(numRows);
        result[0] = {1};
        for(int i = 1; i < numRows; i++)
            for(int j = 0; j <= i; j++)
            {
                int left = j - 1 >= 0? result[i - 1][j - 1]: 0;
                int right =  j < i? result[i - 1][j]: 0;
                result[i].push_back(left + right);
            }
        return result;
    }
};
// @lc code=end
