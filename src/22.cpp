class Solution
{
private:
    // left: 剩余左括号数量，right: 剩余右括号数量
    void dfs(vector<string> &result, string &path, int left, int right)
    {
        if (left < 0 || right < 0 || left > right)
            return;
        if (left == 0 && right == 0)
        {
            result.push_back(path);
            return;
        }
        path.push_back('(');
        dfs(result, path, left - 1, right);
        path.back() = ')';
        dfs(result, path, left, right - 1);
        path.pop_back();
    }

public:
    vector<string> generateParenthesis(int n)
    {
        vector<string> result;
        string path;
        dfs(result, path, n, n);
        return result;
    }
};
