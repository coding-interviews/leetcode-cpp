class Solution
{
private:
    bool isValid(vector<string> &board, int i, int j)
    {
        int n = board.size();
        // 检查当前列是否有重复
        for (int row = 0; row < n; row++)
            if (board[row][j] == 'Q')
                return false;
        // 检查左上->右下对角线是否有重复
        for (int row = i - 1, col = j - 1; row >= 0 && col >= 0; row--, col--)
            if (board[row][col] == 'Q')
                return false;
        for (int row = i + 1, col = j + 1; row < n && col < n; row++, col++)
            if (board[row][col] == 'Q')
                return false;
        // 检查右上->左下对角线是否有重复
        for (int row = i - 1, col = j + 1; row >= 0 && col < n; row--, col++)
            if (board[row][col] == 'Q')
                return false;
        for (int row = i + 1, col = j - 1; row < n && col >= 0; row++, col--)
            if (board[row][col] == 'Q')
                return false;
        return true;
    }

    void solveNQueens(vector<vector<string>> &result, vector<string> &solution, int i)
    {
        int n = solution.size();
        if (i >= n)
            result.push_back(solution);
        else
            for (int j = 0; j < n; j++)
                if (isValid(solution, i, j))
                {
                    char backup = solution[i][j];
                    solution[i][j] = 'Q';
                    solveNQueens(result, solution, i + 1);
                    solution[i][j] = backup;
                }
    }

public:
    vector<vector<string>> solveNQueens(int n)
    {
        vector<vector<string>> result;
        vector<string> solution(n, string(n, '.'));
        solveNQueens(result, solution, 0);
        return result;
    }
};
