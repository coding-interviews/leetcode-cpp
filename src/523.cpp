/*
 * @lc app=leetcode id=523 lang=cpp
 *
 * [523] Continuous Subarray Sum
 */

// @lc code=start
class Solution
{
public:
    bool checkSubarraySum(vector<int> &nums, int k)
    {
        if (k == 0)
        {
            for (int left = 0; left < nums.size() - 1; left++)
                if (nums[left] == 0 && nums[left + 1] == 0)
                    return true;
            return false;
        }
        vector<int> sums(nums.size() + 1, 0);
        partial_sum(nums.begin(), nums.end(), sums.begin() + 1);
        for (int left = 0; left < sums.size(); left++)
            for (int right = left + 2; right < sums.size(); right++)
                if ((sums[right] - sums[left]) % k == 0)
                    return true;
        return false;
    }
};
// @lc code=end
