/*
 * @lc app=leetcode id=421 lang=cpp
 *
 * [421] Maximum XOR of Two Numbers in an Array
 */

// @lc code=start
class Solution
{
private:
    struct TrieNode
    {
        TrieNode *next[2] = {nullptr};
    };

    TrieNode *root;

    void buildTrie(int num)
    {
        TrieNode *p = root;
        for (int i = 31; i >= 0; i--)
        {
            int digit = (num >> i) & 1;
            if (!p->next[digit])
                p->next[digit] = new TrieNode;
            p = p->next[digit];
        }
    }

    int findClosest(int num)
    {
        TrieNode *p = root;
        int result = 0;
        for (int i = 31; i >= 0; i--)
        {
            int digit = (num >> i) & 1;
            if (p->next[digit])
            {
                p = p->next[digit];
                result = (result << 1) | digit;
            }
            else
            {
                p = p->next[!digit];
                result = (result << 1) | !digit;
            }
        }
        return result;
    }

    void delTrie(TrieNode *p)
    {
        if (!p)
            return;
        delTrie(p->next[0]);
        delTrie(p->next[1]);
        delete p;
    }

public:
    int findMaximumXOR(vector<int> &nums)
    {
        // 首先我们建立一棵字典树
        // 与x异或值最大的另一个数为~x，我们在字典树中找到最接近~x的数
        // 其与x的异或值即为该数字与其他数字的最大异或值
        int result = INT_MIN;
        root = new TrieNode;
        for (int num : nums)
            buildTrie(num);
        for (int num : nums)
            result = max(result, num ^ findClosest(~num));
        delTrie(root);
        return result;
    }
};
// @lc code=end
