/*
 * @lc app=leetcode id=397 lang=cpp
 *
 * [397] Integer Replacement
 */

// @lc code=start
class Solution
{
public:
    int integerReplacement(unsigned int n)
    {
        if(n <= 1)
            return 0;
        unordered_map<unsigned int, int> visited;
        queue<unsigned int> q({n});
        unsigned int levels = 0;
        while(!q.empty())
        {
            for(int n = q.size() - 1; n >= 0; n--)
            {
                unsigned int val = q.front();
                q.pop();
                if(val == 1)
                    return levels;
                if(visited[val] || val < 1)
                    continue;
                visited[val] = true;
                if(val % 2 == 0 && !visited[val / 2])
                    q.push(val / 2);
                else if(val % 2 != 0)
                {
                    if(!visited[val + 1])
                        q.push(val + 1);
                    if(!visited[val - 1])
                        q.push(val - 1);
                }
            }
            levels++;
        }
        return levels;
    }
};
// @lc code=end
