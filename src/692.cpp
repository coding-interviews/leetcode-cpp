/*
 * @lc app=leetcode id=692 lang=cpp
 *
 * [692] Top K Frequent Words
 */

// @lc code=start
class Solution
{
public:
    vector<string> topKFrequent(vector<string> &words, int k)
    {
        // 统计频率
        unordered_map<string, int> strToFreq;
        for (const string &str : words)
            strToFreq[str]++;

        // 找出频率最高的k个元素
        struct Compare
        {
            bool operator()(const pair<int, string> &p1, const pair<int, string> &p2) const
            {
                return p1.first > p2.first || p1.first == p2.first && p1.second < p2.second;
            }
        };
        priority_queue<pair<int, string>, vector<pair<int, string>>, Compare> freqToStr;
        for (const pair<string, int> &p : strToFreq)
        {
            freqToStr.push({p.second, p.first});
            if (freqToStr.size() > k)
                freqToStr.pop();
        }

        // 输出结果
        vector<string> result;
        while (!freqToStr.empty())
        {
            result.push_back(freqToStr.top().second);
            freqToStr.pop();
        }
        reverse(result.begin(), result.end());
        return result;
    }
};
// @lc code=end
