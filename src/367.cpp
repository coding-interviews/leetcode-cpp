/*
 * @lc app=leetcode id=367 lang=cpp
 *
 * [367] Valid Perfect Square
 */

// @lc code=start
class Solution
{
private:
    int sqrt(int x)
    {
        long long left = 0, right = x;
        while(left + 1 < right)
        {
            long long mid = left + (right - left) / 2;
            if(mid * mid < x)
                left = mid;
            else if(mid * mid > x)
                right = mid;
            else
                return mid;
        }
        return right * right <= x? right: left;
    }

public:
    bool isPerfectSquare(int num)
    {
        int root = sqrt(num);
        return root * root == num;
    }
};
// @lc code=end
