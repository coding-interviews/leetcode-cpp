/*
 * @lc app=leetcode id=218 lang=cpp
 *
 * [218] The Skyline Problem
 */

// @lc code=start
class Solution
{
public:
    vector<vector<int>> getSkyline(vector<vector<int>> &buildings)
    {
        struct Line
        {
            enum Type
            {
                START,
                END
            };
            int index, height;
            Type type;
            Line(int index, int height, Type type) : index(index), height(height), type(type) {}
        };

        vector<Line> lines;
        lines.reserve(buildings.size() * 2);
        for (const vector<int> &building : buildings)
        {
            lines.push_back({building[0], building[2], Line::START});
            lines.push_back({building[1], building[2], Line::END});
        }
        sort(lines.begin(), lines.end(), [](const Line &l1, const Line &l2) {
            return l1.index < l2.index || l1.index == l2.index && l1.type < l2.type;
        });

        multiset<int> heights;
        heights.insert(0);
        vector<vector<int>> skyline;
        for (const Line &line : lines)
        {
            if (line.type == Line::START)
                heights.insert(line.height);
            else
                heights.erase(heights.find(line.height));
            int maxval = *heights.rbegin();
            if (skyline.empty() || skyline.back()[0] != line.index && skyline.back()[1] != maxval)
                skyline.push_back({line.index, maxval});
            else if (skyline.back()[0] == line.index)
                skyline.back()[1] = max(skyline.back()[1], maxval);
        }
        if (!skyline.empty())
            skyline.back()[1] = 0;
        return skyline;
    }
};
// @lc code=end
