/*
 * @lc app=leetcode id=395 lang=cpp
 *
 * [395] Longest Substring with At Least K Repeating Characters
 */

// @lc code=start
class Solution
{
public:
    int longestSubstring(const string &s, int k)
    {
        int maxlen = 0;
        for(int left = 0; left < s.length(); left++)
        {
            int counter[128] = { 0 }, failed = 0; // 滑动窗内未达到及格线k的字符数量
            for(int right = left; right < s.length(); right++)
            {
                if(counter[s[right]] == 0)
                    failed++;
                counter[s[right]]++;
                if(counter[s[right]] == k)
                    failed--;
                if(failed <= 0)
                    maxlen = max(maxlen, right - left + 1);
            }
            if(counter[s[left]] == k)
                failed++;
            counter[s[left]]--;
            if(counter[s[left]] == 0)
                failed--;
        }
        return maxlen;
    }
};
// @lc code=end
