class Solution
{
public:
    vector<vector<int>> threeSum(vector<int> &nums)
    {
        if (nums.size() < 3)
            return {};
        sort(nums.begin(), nums.end());
        vector<vector<int>> result;
        for (int left = 0; left < nums.size() - 2;)
        {
            for (int mid = left + 1, right = nums.size() - 1; mid < right;)
            {
                int sum = nums[left] + nums[mid] + nums[right];
                if (sum == 0)
                    result.push_back({nums[left], nums[mid], nums[right]});
                if (sum <= 0)
                    do
                        mid++;
                    while (mid < right && nums[mid - 1] == nums[mid]);
                else
                    do
                        right--;
                    while (mid < right && nums[right] == nums[right + 1]);
            }
            do
                left++;
            while (left < nums.size() - 2 && nums[left - 1] == nums[left]);
        }
        return result;
    }
};
