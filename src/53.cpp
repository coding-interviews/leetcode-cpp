class Solution
{
public:
    int maxSubArray(vector<int> &nums)
    {
        int cursum = 0, maxsum = 0;
        for (int num : nums)
        {
            cursum += num;
            cursum = max(cursum, 0);
            maxsum = max(maxsum, cursum);
        }
        return maxsum > 0 || nums.empty() ? maxsum : *max_element(nums.begin(), nums.end());
    }
};
