/*
 * @lc app=leetcode id=169 lang=cpp
 *
 * [169] Majority Element
 */

// @lc code=start
class Solution
{
public:
    int majorityElement(vector<int> &nums)
    {
        unordered_map<int, int> counter;
        for (int num : nums)
            counter[num]++;
        int element, maxcount = 0;
        for (const pair<int, int> &p : counter)
            if (p.second > maxcount)
                element = p.first, maxcount = p.second;
        return element;
    }
};
// @lc code=end
