/*
 * @lc app=leetcode id=365 lang=cpp
 *
 * [365] Water and Jug Problem
 */

// @lc code=start

#ifdef __GNUC__
template <typename T>
inline T gcd(T x, T y)
{
    return __gcd(x, y);
}
#endif

class Solution
{
public:
    bool canMeasureWater(int x, int y, int z)
    {
        if(x == 0 || y == 0)
            return z == x || z == y;
        else
            return z <= x + y && z % gcd(x, y) == 0;
    }
};
// @lc code=end
