/*
 * @lc app=leetcode id=372 lang=cpp
 *
 * [372] Super Pow
 */

// @lc code=start
class Solution
{
private:
    int power(int a, int b)
    {
        if (b == 0)
            return 1;
        int half = power(a % 1337, b / 2) % 1337;
        if (b % 2)
            return half * half % 1337 * a % 1337;
        else
            return half * half % 1337;
    }
public:
	int superPow(int a, vector<int>& b)
    {
        int result = 1;
        for (int i = 0; i < b.size(); i++)
            result = power(result % 1337, 10) * power(a % 1337, b[i]) % 1337;
        return result;
	}
};
// @lc code=end
