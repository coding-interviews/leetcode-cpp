/*
 * @lc app=leetcode id=66 lang=cpp
 *
 * [66] Plus One
 */

// @lc code=start
class Solution
{
public:
    vector<int> plusOne(vector<int>& digits)
    {
        int carry = 1;
        for(int i = digits.size() - 1; i >= 0; i--)
        {
            carry += digits[i];
            digits[i] = carry % 10;
            carry /= 10;
        }
        if(carry)
            digits.insert(digits.begin(), 1);
        return digits;
    }
};
// @lc code=end
