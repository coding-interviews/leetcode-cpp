/*
 * @lc app=leetcode id=233 lang=cpp
 *
 * [233] Number of Digit One
 */

// @lc code=start
class Solution
{
public:
    int countDigitOne(int n)
    {
        int counter = 0;
        for (long long i = 1; i <= n; i *= 10)
        {
            long long divider = i * 10;
            counter += (n / divider) * i + min(max(n % divider - i + 1, 0LL), i);
        }
        return counter;
    }
};
// @lc code=end
