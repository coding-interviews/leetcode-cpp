class Solution
{
public:
    int largestRectangleArea(vector<int> &heights)
    {
        int maxarea = 0;
        stack<int> s;

        for (int i = 0; i <= heights.size(); i++)
        {
            while (!s.empty() && (i == heights.size() || heights[s.top()] >= heights[i]))
            {
                int h = heights[s.top()];
                s.pop();
                int left = !s.empty() ? s.top() + 1 : 0;
                int right = i - 1;
                maxarea = max(maxarea, h * (right - left + 1));
            }
            s.push(i);
        }
        return maxarea;
    }
};
