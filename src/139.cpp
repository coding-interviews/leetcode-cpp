/*
 * @lc app=leetcode id=139 lang=cpp
 *
 * [139] Word Break
 */

// @lc code=start
class Solution
{
public:
    bool wordBreak(string &s, vector<string> &wordDict)
    {
        int N = s.length();
        unordered_set<string> words;
        vector<int> lengths;

        for (const string &word : wordDict)
        {
            words.insert(word);
            lengths.push_back(word.length());
        }

        vector<bool> visited(N + 1, false);
        queue<int> q({0});
        while (!q.empty())
        {
            int i = q.front();
            q.pop();
            if (visited[i])
                continue;
            if (i == N)
                return true;
            visited[i] = true;
            for (int len : lengths)
                if (i + len <= N && !visited[i + len] && words.count(s.substr(i, len)))
                    q.push(i + len);
        }
        return false;
    }
};

// @lc code=end
