/*
 * @lc app=leetcode id=202 lang=cpp
 *
 * [202] Happy Number
 */

// @lc code=start
class Solution
{
private:
    int next(int n)
    {
        int result = 0;
        while(n)
        {
            result += (n % 10) * (n % 10);
            n /= 10;
        }
        return result;
    }

public:
    bool isHappy(int n)
    {
        unordered_set<int> visited;
        while(n != 1 && !visited.count(n))
        {
            visited.insert(n);
            n = next(n);
        }
        return n == 1;
    }
};
// @lc code=end
