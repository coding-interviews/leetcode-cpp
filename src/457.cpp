/*
 * @lc app=leetcode id=457 lang=cpp
 *
 * [457] Circular Array Loop
 */

// @lc code=start
class Solution
{
public:
    bool circularArrayLoop(vector<int> &nums)
    {
        // 检查正向
        for (int start = 0; start < nums.size(); start++)
        {
            vector<bool> visited(nums.size(), false);
            for (int i = start, prev = -1; nums[i] > 0 && prev != i; prev = i, i = (i + nums[i]) % nums.size())
                if (visited[i])
                    return true;
                else
                    visited[i] = true;
        }

        // 检查反向
        for (int start = 0; start < nums.size(); start++)
        {
            vector<bool> visited(nums.size(), false);
            for (int i = start, prev = -1; nums[i] < 0 && prev != i;
                 prev = i, i = (i + nums[i] + nums.size() * 1000) % nums.size())
                if (visited[i])
                    return true;
                else
                    visited[i] = true;
        }

        return false;
    }
};
// @lc code=end
