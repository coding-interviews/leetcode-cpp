/*
 * @lc app=leetcode id=438 lang=cpp
 *
 * [438] Find All Anagrams in a String
 */

// @lc code=start
class Solution
{
public:
    vector<int> findAnagrams(const string &s, const string &p)
    {
        if (p.length() > s.length())
            return {};
        // 统计字符串出现的频率
        int dict[26] = {0};
        for (char c : p)
            dict[c - 'a']++;
        // 开始查找
        vector<int> result;
        int cur[26] = {0};
        for (int i = 0; i < p.length(); i++)
            cur[s[i] - 'a']++;
        if (memcmp(dict, cur, sizeof(dict)) == 0)
            result.push_back(0);
        for (int i = 1; i <= s.length() - p.length(); i++)
        {
            cur[s[i - 1] - 'a']--, cur[s[i + p.length() - 1] - 'a']++;
            if (memcmp(dict, cur, sizeof(dict)) == 0)
                result.push_back(i);
        }
        return result;
    }
};
// @lc code=end
