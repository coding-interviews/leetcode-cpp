/*
 * @lc app=leetcode id=137 lang=cpp
 *
 * [137] Single Number II
 */

// @lc code=start
class Solution
{
public:
    int singleNumber(vector<int> &nums)
    {
        int counter[32] = {0};
        for (int num : nums)
            for (int i = 0; i < 32; i++)
                counter[i] += (num >> i) & 1;
        int result = 0;
        for (int i = 31; i >= 0; i--)
            result = result << 1 | counter[i] % 3;
        return result;
    }
};
// @lc code=end
