/*
 * @lc app=leetcode id=30 lang=cpp
 *
 * [30] Substring with Concatenation of All Words
 */

// @lc code=start
class Solution
{
public:
    vector<int> findSubstring(const string &s, vector<string>& words)
    {
        if (s.empty() || words.empty())
            return {};
        unordered_map<string, int> counter;
        for (const string &word : words)
            counter[word]++;
        vector<int> result;
        for (int i = 0, n = s.length(), m = words.front().length(), k = words.size(); i < m; i++)
        {
            unordered_map<string, int> window;
            // 对区间[i, i + k * m)执行一次完全扫描
            for (int j = 0; j < k; j++)
            {
                string word = s.substr(i + j * m, m);
                if (counter.count(word))
                    window[word]++;
            }
            if (window == counter)
                result.push_back(i);
            // 对区间[i + t * m, i + (t + 1) * m)进行快速操作
            for (int t = 1; i + (t + k) * m <= n; t++)
            {
                string left = s.substr(i + (t - 1) * m, m), right = s.substr(i + (t + k - 1) * m, m);
                if (counter.count(left))
                    window[left]--;
                if (counter.count(right))
                    window[right]++;
                if (window == counter)
                    result.push_back(i + t * m);
            }
        }
        return result;
    }
};
// @lc code=end
