/*
 * @lc app=leetcode id=150 lang=cpp
 *
 * [150] Evaluate Reverse Polish Notation
 */

// @lc code=start
class Solution
{
public:
    int evalRPN(vector<string> &tokens)
    {
        stack<int> s;
        for (const string &token : tokens)
            if (token != "+" && token != "-" && token != "*" && token != "/")
                s.push(atoi(token.c_str()));
            else
            {
                int b = s.top();
                s.pop();
                int a = s.top();
                s.pop();
                if (token == "+")
                    s.push(a + b);
                else if (token == "-")
                    s.push(a - b);
                else if (token == "*")
                    s.push(a * b);
                else
                    s.push(a / b);
            }
        return s.top();
    }
};
// @lc code=end
