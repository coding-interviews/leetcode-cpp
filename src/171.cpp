/*
 * @lc app=leetcode id=171 lang=cpp
 *
 * [171] Excel Sheet Column Number
 */

// @lc code=start
class Solution
{
public:
    int titleToNumber(const string &s)
    {
        int result = 0;
        for(char ch: s)
            result = result * 26 + (ch - 'A') + 1;
        return result;
    }
};
// @lc code=end
