/*
 * @lc app=leetcode id=85 lang=cpp
 *
 * [85] Maximal Rectangle
 */

// @lc code=start
class Solution
{
private:
	int largestRectangleArea(vector<int>& heights)
	{
		int maxarea = 0;
		stack<int> s;

		for (int i = 0; i <= heights.size(); i++)
		{
			while (!s.empty() && (i == heights.size() || heights[s.top()] >= heights[i]))
			{
				int h = heights[s.top()];
				s.pop();
				int left = !s.empty() ? s.top() + 1 : 0;
				int right = i - 1;
				maxarea = max(maxarea, h * (right - left + 1));
			}
			s.push(i);
		}
		return maxarea;
	}

public:
	int maximalRectangle(vector<vector<char>>& matrix)
	{
        if(matrix.empty())
            return 0;
		vector<vector<int>> histograms(matrix.size(), vector<int>(matrix.front().size(), 0));
		for (int j = 0; j < matrix.front().size(); j++)
			histograms.front()[j] = matrix.front()[j] - '0';
		int maxarea = largestRectangleArea(histograms[0]);
		for (int i = 1; i < matrix.size(); i++)
		{
			for (int j = 0; j < matrix[i].size(); j++)
				if (matrix[i][j] != '0')
					histograms[i][j] = histograms[i - 1][j] + 1;
			maxarea = max(maxarea, largestRectangleArea(histograms[i]));
		}
		return maxarea;
	}
};
// @lc code=end
