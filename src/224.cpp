/*
 * @lc app=leetcode id=224 lang=cpp
 *
 * [224] Basic Calculator
 */

// @lc code=start
typedef struct
{
	int index;
	int *data;
} Stack;

void stack_initialize(Stack *self, int size)
{
	self->data = (int *)calloc(size, sizeof(int));
	self->index = 0;
}

void stack_finalize(Stack *self)
{
	free(self->data);
}

void stack_push(Stack *self, int value)
{
	self->data[++self->index] = value;
}

int stack_pop(Stack *self)
{
	return self->data[self->index--];
}

class Solution
{
public:
	int calculate(const string &s)
	{
		int result = 0, operand = 0, sign = 1;
		Stack st;
		stack_initialize(&st, s.length() * 2);
		for (char ch : s)
		{
			if ('0' <= ch && ch <= '9')
				operand = operand * 10 + (ch - '0');
			else if (ch == '+' || ch == '-')
			{
				result += sign * operand;
				operand = 0, sign = ch == '+' ? 1 : -1;
			}
			else if (ch == '(')
			{
				stack_push(&st, result);
				stack_push(&st, sign);
				result = 0, sign = 1;
			}
			else if (ch == ')')
			{
				result += sign * operand;
				result *= stack_pop(&st);
				result += stack_pop(&st);
				operand = 0;
			}
		}
		stack_finalize(&st);
		return result + sign * operand;
	}
};
// @lc code=end
