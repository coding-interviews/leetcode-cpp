/*
 * @lc app=leetcode id=406 lang=cpp
 *
 * [406] Queue Reconstruction by Height
 */

// @lc code=start
/*
 * @lc app=leetcode id=406 lang=cpp
 *
 * [406] Queue Reconstruction by Height
 */
class Solution
{
public:
    vector<vector<int>> reconstructQueue(vector<vector<int>> &people)
    {
        sort(people.begin(), people.end(), [](const vector<int> &p1, const vector<int> &p2) {
            return p1[0] > p2[0] || p1[0] == p2[0] && p1[1] < p2[1];
        });
        vector<vector<int>> result;
        for (vector<int> &person : people)
            result.insert(result.begin() + person[1], person);
        return result;
    }
};

// @lc code=end
