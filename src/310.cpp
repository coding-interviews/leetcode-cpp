/*
 * @lc app=leetcode id=310 lang=cpp
 *
 * [310] Minimum Height Trees
 */

// @lc code=start
static int _ = []()
{
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    return 0;
}();

class Solution
{
public:
	vector<int> findMinHeightTrees(int n, vector<vector<int>>& edges)
	{
		unordered_map<int, vector<int>> graph;
		for (const vector<int>& edge : edges)
		{
			graph[edge[0]].push_back(edge[1]);
			graph[edge[1]].push_back(edge[0]);
		}

		// 统计每个节点的度
		vector<int> degree(n, 0);
		for (int i = 0; i < n; i++)
			degree[i] = graph[i].size();

		// 初始化入度为1的节点
		queue<int> q;
		for (int i = 0; i < n; i++)
			if (degree[i] <= 1)
				q.push(i);

		// 开始拓扑排序
        vector<int> result;
		for (unordered_set<int> visited; !q.empty(); )
        {
            vector<int> level;
            for(int n = q.size() - 1; n >= 0; n--)
            {
                int current = q.front();
                q.pop();
                if (visited.count(current))
                    continue;
                visited.insert(current);
                level.push_back(current);
                for (int next : graph[current])
                {
                    degree[next]--;
                    if (degree[next] <= 1)
                        q.push(next);
                }
            }
            if(!level.empty())
                result = move(level);
        }

		return result;
	}
};
// @lc code=end
