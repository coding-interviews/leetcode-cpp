/*
 * @lc app=leetcode id=213 lang=cpp
 *
 * [213] House Robber II
 */

// @lc code=start
class Solution
{
public:
    int rob(vector<int> &nums)
    {
        if (nums.empty())
            return 0;
        else if (nums.size() == 1)
            return nums[0];

        vector<int> dp(nums.size(), 0);
        // 抢nums[0]
        dp[0] = nums[0], dp[1] = dp[0];
        for (int i = 2; i < nums.size() - 1; i++)
            dp[i] = max(dp[i - 2] + nums[i], dp[i - 1]);
        int result = dp[nums.size() - 2];
        // 不抢nums[0]
        dp[0] = 0, dp[1] = nums[1];
        for (int i = 2; i < nums.size(); i++)
            dp[i] = max(dp[i - 2] + nums[i], dp[i - 1]);
        return max(result, dp.back());
    }
};

// @lc code=end
