/*
 * @lc app=leetcode id=345 lang=cpp
 *
 * [345] Reverse Vowels of a String
 */

// @lc code=start
class Solution
{
private:
    char lower(char ch)
    {
        return 'A' <= ch && ch <= 'Z'? ch - 'A' + 'a': ch;
    }

    bool isVowel(char ch)
    {
        ch = lower(ch);
        return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u';
    }

public:
    string reverseVowels(string s)
    {
        for(int left = 0, right = s.length() - 1; left < right; left++, right--)
        {
            while(left < right && !isVowel(s[left]))
                left++;
            while(left < right && !isVowel(s[right]))
                right--;
            swap(s[left], s[right]);
        }
        return s;
    }
};
// @lc code=end
