/*
 * @lc app=leetcode id=35 lang=cpp
 *
 * [35] Search Insert Position
 */

// @lc code=start
class Solution
{
public:
    int searchInsert(vector<int>& nums, int target)
    {
        if(nums.empty())
            return 0;
        int left = 0, right = nums.size() - 1;
        while(left + 1 < right)
        {
            int mid = left + (right - left) / 2;
            if(nums[mid] < target)
                left = mid;
            else if(nums[mid] > target)
                right = mid;
            else
                return mid;
        }
        if(target <= nums[left])
            return left;
        else if(target > nums[right])
            return right + 1;
        else
            return right;
    }
};
// @lc code=end
