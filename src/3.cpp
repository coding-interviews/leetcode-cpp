class Solution
{
public:
    int lengthOfLongestSubstring(string &s)
    {
        // 同向双指针，右指针为主指针
        int dict[128]; // 存储上一次出现的位置
        memset(dict, -1, sizeof(dict));
        int maxlen = 0;
        for (int left = -1, right = 0; right < s.length(); right++)
        {
            if (dict[s[right]] >= 0)
                left = max(left, dict[s[right]]);
            maxlen = max(maxlen, right - left);
            dict[s[right]] = right;
        }
        return maxlen;
    }
};
