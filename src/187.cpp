/*
 * @lc app=leetcode id=187 lang=cpp
 *
 * [187] Repeated DNA Sequences
 */

// @lc code=start
class Solution
{
public:
    vector<string> findRepeatedDnaSequences(string &s)
    {
        unordered_map<string, int> dict;
        for (int i = 10; i <= s.length(); i++)
            dict[s.substr(i - 10, 10)]++;
        vector<string> result;
        for (const pair<string, int> &p : dict)
            if (p.second > 1)
                result.emplace_back(p.first);
        return result;
    }
};

// @lc code=end
