/*
 * @lc app=leetcode id=36 lang=cpp
 *
 * [36] Valid Sudoku
 */

// @lc code=start
class Solution
{
public:
	bool isValidSudoku(vector<vector<char>>& board)
	{
		// 检查每一行
		for (int i = 0; i < 9; i++)
		{
			bool exist[10] = { false };
			for (int j = 0; j < 9; j++)
			{
				if(board[i][j] == '.')
					continue;
				if (exist[board[i][j] - '0'])
					return false;
				exist[board[i][j] - '0'] = true;
			}
		}
		// 检查每一列
		for (int j = 0; j < 9; j++)
		{
			bool exist[10] = { false };
			for (int i = 0; i < 9; i++)
			{
				if (board[i][j] == '.')
					continue;
				if (exist[board[i][j] - '0'])
					return false;
				exist[board[i][j] - '0'] = true;
			}
		}
		// 检查3x3的方格
		for(int i = 0; i < 9; i+=3)
			for (int j = 0; j < 9; j+=3)
			{
				bool exist[10] = { false };
				for(int x = i; x < i + 3; x++)
					for (int y = j; y < j + 3; y++)
					{
						if(board[x][y] == '.')
							continue;
						if (exist[board[x][y] - '0'])
							return false;
						exist[board[x][y] - '0'] = true;
					}
			}
		return true;
	}
};
// @lc code=end
