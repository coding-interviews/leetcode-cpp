/*
 * @lc app=leetcode id=204 lang=cpp
 *
 * [204] Count Primes
 */

// @lc code=start
class Solution
{
public:
    int countPrimes(int n)
    {
        if(n <= 2)
            return 0;
        bool *isPrime = new bool[n];
        memset(isPrime, true, sizeof(bool) * n);
        for(int i = 2; i * i < n; i++)
            if(isPrime[i])
                for(int j = 2 * i; j < n; j += i)
                    isPrime[j] = false;
        int result = count(isPrime + 2, isPrime + n, true);
        delete []isPrime;
        return result;
    }
};
// @lc code=end
