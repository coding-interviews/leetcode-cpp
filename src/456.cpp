/*
 * @lc app=leetcode id=456 lang=cpp
 *
 * [456] 132 Pattern
 */

// @lc code=start
class Solution
{
public:
    bool find132pattern(vector<int> &nums)
    {
        stack<int> s;
        for (int i = nums.size() - 1, ak = INT_MIN; i >= 0; i--)
            if (nums[i] < ak)
                return true;
            else
            {
                while (!s.empty() && nums[i] > s.top())
                {
                    ak = s.top();
                    s.pop();
                }
                s.push(nums[i]);
            }
        return false;
    }
};

// @lc code=end
