/*
 * @lc app=leetcode id=494 lang=cpp
 *
 * [494] Target Sum
 */

// @lc code=start
class Solution
{
public:
	int findTargetSumWays(vector<int>& nums, int S)
	{
		if (S > 1000)
			return 0;
		vector<vector<int>> dp(nums.size(), vector<int>(2001, 0));
		dp[0][1000 + nums[0]] = 1;
		dp[0][1000 - nums[0]]++;    // 注意nums[0]可能是0
		for (int i = 1; i < nums.size(); i++)
			for (int w = nums[i]; w <= 2000 - nums[i]; w++)
                {
                    dp[i][w - nums[i]] += dp[i - 1][w];
                    dp[i][w + nums[i]] += dp[i - 1][w];
                }
		return dp.back()[S + 1000];
	}
};

// @lc code=end
