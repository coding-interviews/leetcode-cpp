/*
 * @lc app=leetcode id=87 lang=cpp
 *
 * [87] Scramble String
 */

// @lc code=start
class Solution
{
private:
	bool isScramble(const string& s1, const string& s2, int left1, int right1, int left2, int right2)
	{
		if (s1.compare(left1, right1 - left1, s2, left2, right2 - left2) == 0)
			return true;
		int counter[26] = { 0 };
		for (int i = left1, j = left2; i < right1; i++, j++)
			counter[s1[i] - 'a']++, counter[s2[j] - 'a']--;
		if (!all_of(counter, counter + sizeof(counter) / sizeof(int), [](int val) {return val == 0; }))
			return false;
		for (int k = 1; k < right1 - left1; k++)
			if (isScramble(s1, s2, left1, left1 + k, right2 - k, right2) &&
				isScramble(s1, s2, left1 + k, right1, left2, right2 - k) ||
				isScramble(s1, s2, left1, left1 + k, left2, left2 + k) &&
				isScramble(s1, s2, left1 + k, right1, left2 + k, right2))
				return true;
		return false;
	}

public:
	bool isScramble(const string& s1, const string& s2)
	{
		return isScramble(s1, s2, 0, s1.length(), 0, s2.length());
	}
};
// @lc code=end
