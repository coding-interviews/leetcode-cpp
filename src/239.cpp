/*
 * @lc app=leetcode id=239 lang=cpp
 *
 * [239] Sliding Window Maximum
 */

// @lc code=start
class Solution
{
public:
    vector<int> maxSlidingWindow(vector<int> &nums, int k)
    {
        if (nums.empty())
            return {};
        multiset<int> rbtree;
        for (int i = 0; i < k; i++)
            rbtree.insert(nums[i]);
        vector<int> result;
        result.reserve(nums.size() - k + 1);
        result.push_back(*rbtree.rbegin());
        for (int right = k; right < nums.size(); right++)
        {
            rbtree.insert(nums[right]);
            rbtree.erase(rbtree.find(nums[right - k]));
            result.push_back(*rbtree.rbegin());
        }
        return result;
    }
};

// @lc code=end
