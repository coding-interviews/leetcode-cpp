/*
 * @lc app=leetcode id=212 lang=cpp
 *
 * [212] Word Search II
 */

// @lc code=start
class Solution
{
private:
    struct TrieNode
    {
        bool is_word = false;
        TrieNode *next[26] = { nullptr };
    };

    TrieNode *root;

    void buildTrie(const string &word)
    {
        TrieNode *p = root;
        for(char ch: word)
        {
            if(!p->next[ch - 'a'])
                p->next[ch - 'a'] = new TrieNode;
            p = p->next[ch - 'a'];
        }
        p->is_word = true;
    }

    void delTrie(TrieNode *root)
    {
        if(!root)
            return;
        for (int i = 0; i < 26; i++)
            delTrie(root->next[i]);
        delete root;
    }

    void dfs(vector<vector<char>> &board, int i, int j,
             unordered_set<string> &result, string &path, const TrieNode *node)
    {
        int M = board.size(), N = board.front().size();
        if(i < 0 || i >= M || j < 0 || j >= N || board[i][j] == '\0' || !node->next[board[i][j] - 'a'])
            return;
        node = node->next[board[i][j] - 'a'];
        path.push_back(board[i][j]);
        char backup = board[i][j];
        board[i][j] = '\0';
        if(node->is_word)
            result.insert(path);
        dfs(board, i - 1, j, result, path, node);
        dfs(board, i + 1, j, result, path, node);
        dfs(board, i, j - 1, result, path, node);
        dfs(board, i, j + 1, result, path, node);
        board[i][j] = backup;
        path.pop_back();
    }

public:
    vector<string> findWords(vector<vector<char>>& board, vector<string>& words)
    {
        if(board.empty() || board.front().empty())
            return {};
        root = new TrieNode();
        for(const string &word: words)
            buildTrie(word);
        int M = board.size(), N = board.front().size();
        unordered_set<string> result;
        string path;
        for(int i = 0; i < M; i++)
            for(int j = 0; j < N; j++)
                dfs(board, i, j, result, path, root);
        delTrie(root);
        return vector<string>(result.begin(), result.end());
    }
};
// @lc code=end
