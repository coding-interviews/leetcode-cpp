/*
 * @lc app=leetcode id=532 lang=cpp
 *
 * [532] K-diff Pairs in an Array
 */

// @lc code=start
class Solution
{
public:
    int findPairs(vector<int> &nums, int k)
    {
        sort(nums.begin(), nums.end());
        int result = 0;
        for (int left = 0, right = 1; right < nums.size();)
        {
            int diff = nums[right] - nums[left];
            if (diff == k)
                result++;
            if (diff >= k)
            {
                do
                    left++;
                while (left < nums.size() && nums[left - 1] == nums[left]);
                if (left >= right)
                    right = left + 1;
            }
            else
            {
                do
                    right++;
                while (right < nums.size() && nums[right - 1] == nums[right]);
            }
        }
        return result;
    }
};
// @lc code=end
