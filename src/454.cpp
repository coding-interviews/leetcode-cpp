/*
 * @lc app=leetcode id=454 lang=cpp
 *
 * [454] 4Sum II
 */

// @lc code=start
class Solution
{
private:
    unordered_map<int, int> toCounter(const vector<int> &A, const vector<int> &B)
    {
        unordered_map<int, int> counter;
        for(int a: A)
            for(int b: B)
                counter[a + b]++;
        return counter;
    }

public:
    int fourSumCount(vector<int>& A, vector<int>& B, vector<int>& C, vector<int>& D)
    {
        unordered_map<int, int> AB = toCounter(A, B);

        int result = 0;
        for(int c: C)
            for(int d: D)
            {
                auto itAB = AB.find(-c - d);
                if(itAB != AB.end())
                    result += itAB->second;
            }
        return result;
    }
};
// @lc code=end
