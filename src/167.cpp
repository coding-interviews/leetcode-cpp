/*
 * @lc app=leetcode id=167 lang=cpp
 *
 * [167] Two Sum II - Input array is sorted
 */

// @lc code=start
class Solution
{
public:
    vector<int> twoSum(vector<int>& numbers, int target)
    {
        for(int i = 0; i < numbers.size(); i++)
        {
            int right = lower_bound(numbers.begin() + i + 1, numbers.end(), target - numbers[i])
                      - numbers.begin();
            if(right < numbers.size() && numbers[i] + numbers[right] == target)
                return {i + 1, right + 1};
        }
        return {-1, -1};
    }
};
// @lc code=end
