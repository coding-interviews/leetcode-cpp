/*
 * @lc app=leetcode id=539 lang=cpp
 *
 * [539] Minimum Time Difference
 */

// @lc code=start
class Solution
{
private:
	int timeToMin(const string &t)
	{
		int h = (t[0] - '0') * 10 + t[1] - '0';
		int m = (t[3] - '0') * 10 + t[4] - '0';
		return h * 60 + m;
	}

public:
	int findMinDifference(vector<string> &timePoints)
	{
		set<int> timeline;
		for (const string &time : timePoints)
		{
			int minutes = timeToMin(time);
			if (timeline.count(minutes))
				return 0;
			else
				timeline.insert(minutes);
		}

		int mindiff = INT_MAX;
		auto left = timeline.begin(), right = left;
		for (++right; right != timeline.end(); ++right)
		{
			mindiff = min(mindiff, *right - *left);
			left = right;
		}
		mindiff = min(mindiff, *timeline.begin() - *timeline.rbegin() + 24 * 60);
		return mindiff;
	}
};
// @lc code=end
