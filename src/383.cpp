/*
 * @lc app=leetcode id=383 lang=cpp
 *
 * [383] Ransom Note
 */

// @lc code=start
class Solution
{
public:
    bool canConstruct(const string &ransomNote, const string &magazine)
    {
        int counter[128] = { 0 };
        for(char ch: magazine)
            counter[ch]++;
        for(char ch: ransomNote)
            if(--counter[ch] < 0)
                return false;
        return true;
    }
};
// @lc code=end
