class Solution
{
public:
    bool search(vector<int> &nums, int target)
    {
        if (nums.empty())
            return false;
        int back = nums.back();
        if (back == target)
            return true;
        // 由于结尾可能和开头元素相同，我们先把结尾的元素删掉，使得开头严格大于结尾
        while (!nums.empty() && nums.back() == back)
            nums.pop_back();
        if (nums.empty())
            return false;
        int left = 0, right = nums.size() - 1;
        while (left + 1 < right)
        {
            int mid = left + (right - left) / 2;
            if (nums[mid] > nums.back()) // 位于左半段
                if (nums.back() < target && target < nums[mid])
                    right = mid;
                else
                    left = mid;
            else if (nums[mid] < target && target <= nums.back())
                left = mid;
            else
                right = mid;
        }
        return nums[left] == target || nums[right] == target;
    }
};
