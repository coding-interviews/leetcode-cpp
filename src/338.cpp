/*
 * @lc app=leetcode id=338 lang=cpp
 *
 * [338] Counting Bits
 */

// @lc code=start
class Solution
{
private:
    int popcount(int num)
    {
        int count = 0;
        while(num)
        {
            num = num & (num - 1);
            count++;
        }
        return count;
    }

public:
    vector<int> countBits(int num)
    {
        vector<int> result;
        for(int i = 0; i <= num; i++)
            result.push_back(popcount(i));
        return result;
    }
};
// @lc code=end
