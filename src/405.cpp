/*
 * @lc app=leetcode id=405 lang=cpp
 *
 * [405] Convert a Number to Hexadecimal
 */

// @lc code=start
class Solution
{
public:
    string toHex(int num)
    {
        char result[9];
        sprintf(result, "%x", num);
        return string(result);
    }
};
// @lc code=end
