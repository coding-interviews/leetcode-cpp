/*
 * @lc app=leetcode id=115 lang=cpp
 *
 * [115] Distinct Subsequences
 */

// @lc code=start
class Solution
{
public:
    int numDistinct(const string &s, const string &t)
    {
        // 双序列型动态规划
        // 设dp[m][n]表示s[:m]和t[:n]能构成的不同序列数量
        // dp[m][n] = (s[m-1] == t[n-1]? dp[m-1][n] + dp[m-1][n-1]: 0
        //          + (s[m-1] != t[n-1]? dp[m-1][n]: 0)
        vector<unsigned int> dp(t.length() + 1, 0);
        dp[0] = 1;
        for(int i = 1; i <= s.length(); i++)
            for(int j = t.length(); j > 0; j--)
                if(s[i - 1] == t[j - 1])
                    dp[j] += dp[j - 1];
        return dp.back();
    }
};
// @lc code=end
