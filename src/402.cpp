/*
 * @lc app=leetcode id=402 lang=cpp
 *
 * [402] Remove K Digits
 */

// @lc code=start
class Solution
{
public:
    string removeKdigits(const string &num, int k)
    {
        string s;
        for(char ch: num)
        {
            while(k > 0 && !s.empty() && s.back() > ch)
            {
                s.pop_back();
                k--;
            }
            s.push_back(ch);
        }
        size_t begin = s.find_first_not_of('0'), end = s.length() - k;
        return begin != string::npos && begin < end? s.substr(begin, end - begin): "0";
    }
};
// @lc code=end
