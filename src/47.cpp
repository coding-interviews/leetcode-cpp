class Solution
{
private:
    bool next_permutation(vector<int> &nums)
    {
        // 找出第一个左<右的位置
        int left = nums.size() - 2;
        while (nums[left] >= nums[left + 1])
        {
            left--;
            if (left < 0)
                return false; // 已经是最后一个排列
        }
        // 从右往左找出第一个>left的位置
        int right = nums.size() - 1;
        while (left < right && nums[left] >= nums[right])
            right--;
        // 交换left和right
        swap(nums[left], nums[right]);
        // 逆序left以后的数字
        reverse(nums.begin() + left + 1, nums.end());
        return true;
    }

public:
    vector<vector<int>> permuteUnique(vector<int> &nums)
    {
        if (nums.size() <= 1)
            return {nums};
        sort(nums.begin(), nums.end());
        vector<vector<int>> result;
        do
            result.push_back(nums);
        while (next_permutation(nums));
        return result;
    }
};
