/*
 * @lc app=leetcode id=401 lang=cpp
 *
 * [401] Binary Watch
 */

// @lc code=start
class Solution
{
private:

    void dfs(vector<pair<unsigned, unsigned>> &result, unsigned &hour, unsigned &minute,
             int i, int num)
    {
        if(i == 10)
        {
            if(num == 0 && hour < 12 && minute < 60)
                result.push_back(make_pair(hour, minute));
            return;
        }
        dfs(result, hour, minute, i + 1, num);
        // 第i位已经点亮
        if(i < 4 && ((hour >> i) & 1) || i >= 4 && ((minute >> (i - 4)) & 1))
            return;
        // 尝试点亮第i位
        unsigned next_hour = i < 4? (hour | (1 << i)): hour;
        unsigned next_minute = i >= 4? (minute | (1 << (i - 4))): minute;
        dfs(result, next_hour, next_minute, i + 1, num - 1);
    }

public:
    vector<string> readBinaryWatch(int num)
    {
        unsigned hour = 0, minute = 0;
        vector<pair<unsigned, unsigned>> times;
        dfs(times, hour, minute, 0, num);

        vector<string> result;
        for(const auto &p: times)
        {
            string h = to_string(p.first);
            string m = p.second < 10? string("0") + to_string(p.second): to_string(p.second);
            result.push_back(h + ":" + m);
        }
        return result;
    }
};
// @lc code=end
