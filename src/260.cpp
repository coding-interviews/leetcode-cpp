/*
 * @lc app=leetcode id=260 lang=cpp
 *
 * [260] Single Number III
 */

// @lc code=start
class Solution
{
public:
    vector<int> singleNumber(vector<int>& nums)
    {
        int xorVal = 0;
        for(int num: nums)
            xorVal ^= num;
        int tester = (xorVal & (xorVal - 1)) ^ xorVal;
        int number1 = 0, number2 = 0;
        for(int num: nums)
            if(num & tester)
                number1 ^= num;
            else
                number2 ^= num;
        return {number1, number2};
    }
};
// @lc code=end
