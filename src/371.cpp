/*
 * @lc app=leetcode id=371 lang=cpp
 *
 * [371] Sum of Two Integers
 */

// @lc code=start
class Solution
{
public:
    int getSum(int a, int b)
    {
        unsigned sum = (unsigned)a ^ (unsigned)b, carry = ((unsigned)a & (unsigned)b) << 1;
        while(carry)
        {
            int next_carry = (sum & carry) << 1;
            sum ^= carry;
            carry = next_carry;
        }
        return sum;
    }
};
// @lc code=end
