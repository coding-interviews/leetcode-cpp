/*
 * @lc app=leetcode id=396 lang=cpp
 *
 * [396] Rotate Function
 */

// @lc code=start
class Solution
{
public:
    int maxRotateFunction(vector<int>& A)
    {
        long long sum = accumulate(A.begin(), A.end(), 0ll);
        long long f = 0;
        for(long long i = 0; i < A.size(); i++)
            f += i * A[i];
        long long maxval = f;
        for(long long k = 1; k < A.size(); k++)
            f += sum - A.size() * A[A.size() - k], maxval = max(maxval, f);
        return maxval;
    }
};

// F(0) = sum i         * A[i]
// F(1) = sum (i+1) % N * A[i]
// F(k) = sum (i+k) % N * A[i]
// F(k) - F(k - 1) = A[0] + ... + A[N-1] - N * A[n-k]

// 例如
// F(0) = (0 * 4) + (1 * 3) + (2 * 2) + (3 * 6) = 0 + 3 + 4 + 18
// F(1) = (1 * 4) + (2 * 3) + (3 * 2) + (0 * 6) = 4 + 6 + 6 + 6
// F(1) - F(0) = 4 + 3 + 2 - (N - 1) * 6 = -9
// @lc code=end
