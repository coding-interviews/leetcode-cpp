/*
 * @lc app=leetcode id=547 lang=cpp
 *
 * [547] Friend Circles
 */

// @lc code=start
class Solution
{
private:
    vector<int> uf;

    int find(int x)
    {
        if (uf[x] < 0)
            return x;
        else
            return uf[x] = find(uf[x]);
    }

public:
    int findCircleNum(vector<vector<int>> &M)
    {
        uf.resize(M.size(), -1);
        for (int i = 0; i < M.size(); i++)
            for (int j = i + 1; j < M.size(); j++)
                if (M[i][j])
                {
                    int root1 = find(i), root2 = find(j);
                    if (root1 != root2)
                        uf[root1] = root2;
                }
        return count(uf.begin(), uf.end(), -1);
    }
};

// @lc code=end
