/*
 * @lc app=leetcode id=312 lang=cpp
 *
 * [312] Burst Balloons
 */

// @lc code=start
class Solution
{
public:
	int maxCoins(vector<int> &nums)
	{
		if (nums.empty())
			return 0;
		// 首先在nums前后各补一个1
		// 设dp[i][j]表示扎爆i+1~j-1气球后的最大收益
		// 初始状态dp[i][i+1] = 0
		// dp[i][j] = max_k { dp[i][k] + dp[k][j] + nums[i] * nums[k] * nums[j] }
		// 其中，i < k < j。那么，dp[i][k]在dp[i][j]左边，dp[k][j]在dp[i][j]下面
		// 所以，从左下角开始计算，从左向右，从下向上
		nums.insert(nums.begin(), 1);
		nums.push_back(1);
		vector<vector<int>> dp(nums.size(), vector<int>(nums.size(), 0));
		for (int i = nums.size() - 1; i >= 0; i--)
			for (int j = i + 2; j < nums.size(); j++)
				for (int k = i + 1; k <= j - 1; k++)
					dp[i][j] = max(dp[i][j], dp[i][k] + dp[k][j] + nums[i] * nums[k] * nums[j]);
		return dp.front().back();
	}
};
// @lc code=end
