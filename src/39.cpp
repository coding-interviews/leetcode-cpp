class Solution
{
private:
    vector<vector<int>> result;
    vector<int> candidates;
    int target;

    void dfs(vector<int> &path, int &sum, int i)
    {
        if (sum == target)
            result.push_back(path);
        else if (sum < target)
        {
            for (int next = i; next < candidates.size(); next++)
            {
                path.push_back(candidates[next]);
                sum += candidates[next];
                dfs(path, sum, next);
                sum -= candidates[next];
                path.pop_back();
            }
        }
    }

public:
    vector<vector<int>> combinationSum(vector<int> &candidates, int target)
    {
        sort(candidates.begin(), candidates.end());
        this->candidates = move(candidates);
        this->target = target;
        vector<int> path;
        int sum = 0;
        dfs(path, sum, 0);
        return result;
    }
};
